<?php
/*
 * Imaginary Media Template functions and definitions
 *
 * @package WordPress
 * @subpackage Imaginary Media
 * @since Imaginary Media 2.0
 *
 */

// Initiate custom functions
add_action( 'after_setup_theme', 'im_setup' );

// General Cleanup
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links_extra');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');

function cp_create_course_post() {
	register_post_type('course_post',
		array(
			'labels' => array(
				'name' => 'Course',
				'singular_name' => 'Course'
			),
			'public' => true,
			'show_ui' => true,
			'supports'	=>	array('title', 'editor', 'page-attributes', 'thumbnail', 'category'),
			'has_archive' => true,
			'rewrite' => true
		));
}
add_action('init', 'cp_create_course_post');

function cp_create_committee_member_post() {
	register_post_type('committee-members',
		array(
			'labels' => array (
				'name' => 'Committee Members',
				'singular_name' => 'Committee Members'
			),
			'public' => true,
			'show_ui' => true,
			'capability_type' => 'page',
			'hierarchical' => true,
			'supports'	=>	array('title', 'editor', 'page-attributes', 'thumbnail', 'category'),
			'rewrite' => true
		));
}
add_action('init', 'cp_create_committee_member_post');

function cp_create_resources_post() {
	register_post_type('resources', array(
		'labels' => array(
			'name' => 'Resources',
			'singular_name' => 'Resources'
		),
		'public' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'show_ui' => true,
		'rewrite' => true,
		'has_archive' => false,
		'taxonomies' => array(
			'post_tag'
		)
	));
}

function tax_chapter_init() {
	register_taxonomy('chapter', 'resources', array(
		'label' => 'Chapter',
		'show_ui' => true
	));
}

function tax_resource_type_init() {
	register_taxonomy('resource_type', 'resources', array(
		'label' => 'Resource Type',
		'show_ui' => true
	));
}

add_action('init', 'cp_create_resources_post');
add_action('init', 'tax_chapter_init');
add_action('init', 'tax_resource_type_init');

function resources_template_chooser($template)
{
	global $wp_query;
	$post_type = get_query_var('post_type');
	if( $wp_query->is_search && $post_type == 'resources' )
	{
		return locate_template('search-resources.php');  //  redirect to archive-search.php
	}
	return $template;
}
add_filter('template_include', 'resources_template_chooser');

// Filter to replace the '=' to 'LIKE'
function my_course_post_where( $where ) {
	$where = str_replace("meta_key = 'course_session_%", "meta_key LIKE 'course_session_%", $where);
	return $where;
}
add_filter('posts_where', 'my_course_post_where');

if ( ! function_exists( 'im_setup' ) ):

	/* ##################################### SETS UP THEME DEFAULTS AND REGISTERS SUPPORT FOR VARIOUS WORDPRESS FEATURES ##################################### */

	function im_setup() {

		// Set timezone to Sydney Australia for date functions
		date_default_timezone_set('Australia/Sydney');

		// This theme uses wp_nav_menu() with the following registered menus
		register_nav_menu( 'primary', 'Primary Menu' );
		//register_nav_menu( 'base', 'Footer Menu' );

		// Add support for thumbnails and custom image sizes
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'gallery-home', 460, 350, false );
		add_image_size( 'gallery-side', 320, 400, false );

		// Add custom editor styles
		add_editor_style();

		// Gets the ID of a page based on a slug
		if ( ! function_exists( 'get_id_by_slug' ) ) :
			function get_id_by_slug($page_slug) {
				$page = get_page_by_path($page_slug);
				if ($page) {
					$page_id = $page->ID;
					return $page_id;
				} else {
					return null;
				}
			}
		endif;

		// Checks for subpage and returns ID of parent
		function is_subpage() {
			global $post;
			if ( is_page() && $post->post_parent ) {
				return $post->post_parent;
			} else {
				return false;
			}
		}

		// Returns parent slug
		function the_parent_slug() {
			global $post;
			if($post->post_parent == 0) return '';
			$post_data = get_post($post->post_parent);
			return $post_data->post_name;
		}

		function addUploadMimes($mimes) {
			$mimes = array_merge($mimes, array(
				'epub|mobi' => 'application/octet-stream'
			));
			return $mimes;
		}
		add_filter('upload_mimes', 'addUploadMimes');

		/* Custom Login Functions ===================================================================================== */
		// Custom logo
		function custom_login_logo() {
			echo '<style type="text/css">
					#login { padding-top: 0 !important;}
					h1 { width: 320px !important; height: 200px !important; }
					h1 a { background:url('.get_bloginfo('template_directory').'/_img/ags.png) 50% 50% no-repeat !important; background-size: 155px 138px !important; display: block; height: 200px !important; width: 320px !important!; }
				</style>';
		}
		add_action('login_head', 'custom_login_logo');

		// Custom login URL
		function custom_login_url(){
			return ('http://australiangeomechanics.org/');
		}
		add_filter('login_headerurl', 'custom_login_url');

		// Custom login URL Title Attribute
		function custom_login_title(){
			return ('Australian Geomechanics Society');
		}
		add_filter('login_headertitle', 'custom_login_title');

		// Add category slugs in body and post class
		function category_id_class($classes) {
			global $post;
			foreach((get_the_category($post->ID)) as $category)
				$classes[] = $category->category_nicename;
			return $classes;
		}
		add_filter('post_class', 'category_id_class');
		add_filter('body_class', 'category_id_class');

		// Add extra profile fields
		add_action( 'show_user_profile', 'ags_profile_fields' );
		add_action( 'edit_user_profile', 'ags_profile_fields' );

		function ags_profile_fields( $user ) { ?>

			<h3>Member Information</h3>

			<table class="form-table">
				<tr>
					<th>Chapter</th>
					<td>
						<ul class="radio">
							<li><label><input type="radio" <?php if (get_the_author_meta( 'chapter', $user->ID) == 'Queensland' ) { ?>checked="checked"<?php }?> name="chapter" value="Queensland"> Queensland</label></li>
							<li><label><input type="radio" <?php if (get_the_author_meta( 'chapter', $user->ID) == 'Newcastle' ) { ?>checked="checked"<?php }?> name="chapter" value="Newcastle"> Newcastle, NSW</label></li>
							<li><label><input type="radio" <?php if (get_the_author_meta( 'chapter', $user->ID) == 'Sydney' ) { ?>checked="checked"<?php }?> name="chapter" value="Sydney"> Sydney, NSW</label></li>
							<li><label><input type="radio" <?php if (get_the_author_meta( 'chapter', $user->ID) == 'Victoria' ) { ?>checked="checked"<?php }?> name="chapter" value="Victoria"> Victoria</label></li>
							<li><label><input type="radio" <?php if (get_the_author_meta( 'chapter', $user->ID) == 'Tasmania' ) { ?>checked="checked"<?php }?> name="chapter" value="Tasmania"> Tasmania</label></li>
							<li><label><input type="radio" <?php if (get_the_author_meta( 'chapter', $user->ID) == 'South Australia' ) { ?>checked="checked"<?php }?> name="chapter" value="South Australia"> South Australia &amp; NT</label></li>
							<li><label><input type="radio" <?php if (get_the_author_meta( 'chapter', $user->ID) == 'Western Australia' ) { ?>checked="checked"<?php }?> name="chapter" value="Western Australia"> Western Australia</label></li>
							<li><label><input type="radio" <?php if (get_the_author_meta( 'chapter', $user->ID) == 'Overseas' ) { ?>checked="checked"<?php }?> name="chapter" value="Overseas"> Overseas</label></li>
						</ul>
					</td>
				</tr>
				<tr>
					<th><label for="company">Company</label></th>
					<td>
						<input type="text" name="company" id="company" value="<?php echo esc_attr( get_the_author_meta( 'company', $user->ID ) ); ?>" class="regular-text" /><br />
						<span class="description">Please enter your employer name leave blank if you are a student</span>
					</td>
				</tr>
				<tr>
					<th><label for="address">Address</label></th>
					<td>
						<textarea name="address" id="address" class="regular-text"><?php echo esc_attr( get_the_author_meta( 'address', $user->ID ) ); ?></textarea><br />
						<span class="description">Please enter your postal address</span>
					</td>
				</tr>
				<tr>
					<th><label for="committee-member">Committee Member</label></th>
					<td>
						<input type="checkbox" name="committee-member" id="committee-member" value="committee-member" <?php if(get_the_author_meta('committee-member', $user->ID)=='committee-member') { echo 'checked="checked"'; } ?>>
						<span class="description">Please tick if user is an committee member</span>
					</td>
				</tr>
			</table>
		<?php }

		add_action( 'personal_options_update', 'ags_save_profile_fields' );
		add_action( 'edit_user_profile_update', 'ags_save_profile_fields' );

		function ags_save_profile_fields( $user_id ) {

			if ( !current_user_can( 'edit_user', $user_id ) )
				return false;

			// Copy and paste this line for additional fields
			update_user_meta( $user_id, 'chapter', $_POST['chapter'] );
			update_user_meta( $user_id, 'company', $_POST['company'] );
			update_user_meta( $user_id, 'address', $_POST['address'] );
			update_user_meta( $user_id, 'committee-member', $_POST['committee-member'] );
		}

		/* Profile options for non-Administrators ======================== */
		if (!current_user_can('manage_options')) {

			// Remove WP button
			function remove_admin_bar_links() {
				global $wp_admin_bar;
				$wp_admin_bar->remove_menu('wp-logo');
			}
			add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );

			// Remove Dashboard from menu
			function remove_menus () {
				global $menu;
				$restricted = array(__('Dashboard'));
				end ($menu);
				while (prev($menu)){
					$value = explode(' ',$menu[key($menu)][0]);
					if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
				}
			}
			add_action('admin_menu', 'remove_menus');

			// Remove colour, visual editor, keyboard shortcuts, and toolbar options
			remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );
			if ( ! function_exists( 'cor_remove_personal_options' ) ) {
			  function cor_remove_personal_options( $subject ) {
				$subject = preg_replace( '#<h3>Personal Options</h3>.+?/table>#s', '', $subject, 1 );
				return $subject;
			  }
			  function cor_profile_subject_start() {
				ob_start( 'cor_remove_personal_options' );
			  }
			  function cor_profile_subject_end() {
				ob_end_flush();
			  }
			}
			add_action( 'admin_head-profile.php', 'cor_profile_subject_start' );
			add_action( 'admin_footer-profile.php', 'cor_profile_subject_end' );

			// Hide Admin Bar from users on site
			add_filter('show_admin_bar', '__return_false');

			// Remove profile fields
			function remove_contactmethod( $contactmethods ) {
				unset($contactmethods['url']);
				unset($contactmethods['aim']);
				unset($contactmethods['jabber']);
				unset($contactmethods['yim']);
				$contactmethods['phone'] = 'Phone number';
				return $contactmethods;
			}
			add_filter('user_contactmethods','remove_contactmethod',10,1);

			add_filter( 'contextual_help', 'ags_remove_help', 999, 3 );
			function ags_remove_help($old_help, $screen_id, $screen){
				$screen->remove_help_tabs();
				return $old_help;
			}

			// Change the Admin welcome message
			function rename_howdy_to_hello( $links, $current_user ) {
				$links[ 5 ] = sprintf( __( 'Hello, %1$s' ), $current_user->user_nicename );
				return $links;
			}
			add_filter( 'admin_user_info_links', 'rename_howdy_to_hello', 10, 2 );

		}

	}

endif;

// search filter
//function tags_search_filter($query) {
//
//  if ( !$query->is_admin && $query->is_search) {
//    $query->set('post__not_in', array(40, 9) ); // id of page or post
//  }
//
//  return $query;
//}
//add_filter( 'pre_get_posts', 'tags_search_filter' );

/**
 * ========================================================================
 * Custom search functionality (to allow search from tags and categories)
 * ========================================================================
 *
 * Added by James Heazlewood, Future Medium, Jan 2015
 *
 * Reference: http://www.rfmeier.net/include-category-and-post-tag-names-in-the-wordpress-search/
 */

// creates SQL that left-joins taxonomies onto search SQL
/*
function custom_posts_join($join, $query)
{
  global $wpdb;

  // if main query and search...
  if(is_main_query() && is_search())
  {
    // join term_relationships, term_taxonomy, and terms into the current SQL where clause
    $join .= "
        LEFT JOIN
        (
            {$wpdb->term_relationships}
            INNER JOIN
                {$wpdb->term_taxonomy} ON {$wpdb->term_taxonomy}.term_taxonomy_id = {$wpdb->term_relationships}.term_taxonomy_id
            INNER JOIN
                {$wpdb->terms} ON {$wpdb->terms}.term_id = {$wpdb->term_taxonomy}.term_id
        )
        ON {$wpdb->posts}.ID = {$wpdb->term_relationships}.object_id ";
  }

  return $join;
}
add_filter( 'posts_join', 'custom_posts_join', 10, 2 );
// append an extra where clause that includes categories and post tags
function custom_posts_where($where, $query)
{
  global $wpdb;

  if( is_main_query() && is_search() )
  {
    // get additional where clause for the user
    $user_where = get_user_posts_where();

    $where .= " OR (
                  {$wpdb->term_taxonomy}.taxonomy IN( 'category', 'post_tag' )
                  AND
                  {$wpdb->terms}.name LIKE '%" . esc_sql( get_query_var( 's' ) ) . "%'
                  {$user_where}
                )";
  }

  return $where;
}
add_filter( 'posts_where', 'custom_posts_where', 10, 2 );
*/

// append an extra where clause that includes categories and post tags
//function custom_posts_where($where)
//{
//  global $wpdb;
//
//  //if( is_main_query() && is_search() )
//  //{
//  //  $where .= " OR (
//  //    {$wpdb->term_taxonomy}.taxonomy = 'post_tag'
//  //    AND
//  //    {$wpdb->terms}.slug LIKE '%" . esc_sql( get_query_var( 's' ) ) . "%'
//  //  )";
//
//  $where .= " AND {$wpdb->terms}.slug LIKE '%" . esc_sql( get_query_var( 's' ) ) . "%'";
//
//  //}
////echo esc_sql( get_query_var( 's' ) );
//echo $where;
//die();
//
//  return $where;
//}
//add_filter( 'posts_where', 'custom_posts_where' );

/**
 * Get a where clause dependent on the current user's status.
 *
 * @uses get_current_user_id()
 * @link http://codex.wordpress.org/Function_Reference/get_current_user_id
 *
 * @return string The user where clause.
 */
/*
function get_user_posts_where()
{
  global $wpdb;

  $user_id = get_current_user_id();
  $sql     = '';
  $status  = array( "'publish'" );

  if( 0 !== $user_id )
  {
    $status[] = "'private'";

    $sql .= " AND {$wpdb->posts}.post_author = " . absint( $user_id );
  }

  $sql .= " AND {$wpdb->posts}.post_status IN( " . implode( ',', $status ) . " ) ";
  //$sql .= " AND {$wpdb->posts}.post_type IN('" . esc_sql( get_query_var( 'post_type' ) ) . "') ) ";

  return $sql;
}

// aggregate all the post IDs that will fall into out query to match the where clause.
function custom_posts_groupby( $groupby, $query )
{
  global $wpdb;

  // if is main query and a search...
  if( is_main_query() && is_search() )
  {
    // assign the GROUPBY
    $groupby = "{$wpdb->posts}.ID";
  }

  return $groupby;
}
add_filter('posts_groupby', 'custom_posts_groupby', 10, 2);
*/
function ags_numeric_posts_nav() {

	if( is_singular() ) return;

	global $wp_query;

    /** Stop execution if there's only 1 page */
  if( $wp_query->max_num_pages <= 1 ) return;

  $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
  $max   = intval( $wp_query->max_num_pages );

  /**	Add current page to the array */
  if ( $paged >= 1 ) $links[] = $paged;

  /**	Add the pages around the current page to the array */
  if ( $paged >= 3 ) {
    $links[] = $paged - 1;
    $links[] = $paged - 2;
  }

  if ( ( $paged + 2 ) <= $max ) {
    $links[] = $paged + 2;
    $links[] = $paged + 1;
  }

  echo '<div class="navigation"><ul>' . "\n";

  /**	Previous Post Link */
  if ( get_previous_posts_link() )
  {
    printf( '<li class="prev">%s</li>' . "\n", get_previous_posts_link('Prev') );
  }
  else
  {
    echo '<li class="prev"><a class="disabled " href="#">Prev</a></li>';
  }

  /**	Link to first page, plus ellipses if necessary */
  if ( ! in_array( 1, $links ) )
  {
    $class = 1 == $paged ? ' class="active"' : '';

    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

    if ( ! in_array( 2, $links ) ) echo '<li>…</li>';
  }

  /**	Link to current page, plus 2 pages in either direction if necessary */
  sort( $links );
  foreach ( (array) $links as $link ) {
    $class = $paged == $link ? ' class="active"' : '';
    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
  }

  /**	Link to last page, plus ellipses if necessary */
  if ( ! in_array( $max, $links ) ) {
    if ( ! in_array( $max - 1, $links ) )
      echo '<li class="dotdot">…</li>' . "\n";

    $class = $paged == $max ? ' class="active"' : '';
    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
  }

  /**	Next Post Link */
  if ( get_next_posts_link() )
  {
    printf( '<li class="next">%s</li>' . "\n", get_next_posts_link('Next') );
  }
  else
  {
    echo '<li><a class="disabled" href="#">Next</a></li>';
  }

  echo '</ul></div>' . "\n";

}

// all this just to make the tabs at the top drop out when you log in.....
function prefix_set_just_logged_in( $username, $user )
{
  update_user_meta( $user->ID, 'user_just_logged_in', true );
}
add_action( 'wp_login', 'prefix_set_just_logged_in', 10, 2 );

function my_login_redirect( $redirect_to, $request, $user ) {
    //is there a user to check?
    global $user;
    if ( isset( $user->roles ) && is_array( $user->roles ) ) {
        return "http://australiangeomechanics.org/";
    } else {
        return $redirect_to;
    }
}

add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );


function prefix_print_local_storage_script()
{
  if(get_user_meta( get_current_user_id(), 'user_just_logged_in' ) == true) {
    //die();
    echo '<script>window.localStorage.setItem(\'TabOpen\', \'true\');</script>';
    delete_user_meta( get_current_user_id(), 'user_just_logged_in' );
  }
}
add_action( 'wp_print_scripts', 'prefix_print_local_storage_script' );


function cc_hide_admin_bar()
{
  if(!current_user_can('edit_posts'))
  {
    show_admin_bar(false);
  }
}
add_action('set_current_user', 'cc_hide_admin_bar');

// Manager non-logged in users or wrong region users
function check_login() {
	// User not logged in
	if(!is_user_logged_in()) {
		echo '<h2>Please login or join this chapter to access this content</h2>>';

	}
}

function pm_string_to_format_date($rawdate, $format = 'd M Y') {
	$y = substr($rawdate,0,4);
	$m = substr($rawdate,4,2);
	$d = substr($rawdate,6,2);
	$rawdate = $d.'-'.$m.'-'.$y;
	$eventDate = date($format, strtotime($rawdate));
	return $eventDate;
}

function get_category_tag($categoryName) {
	$outTag = '';

	if ($categoryName == 'Meeting') {
		$outTag = '<span class="tag tag-meeting">Meeting</span>';
	} elseif ($categoryName == 'Video') {
		$outTag = '<span class="tag tag-video">Video</span>';
	} elseif ($categoryName == 'Download') {
		$outTag = '<span class="tag tag-download">Download</span>';
	} elseif ($categoryName == 'Resource') {
		$outTag = '<span class="tag tag-download">Resource</span>';
	} elseif ($categoryName == 'News') {
		$outTag = '<span class="tag tag-news">News</span>';
	} elseif ($categoryName == 'Event') {
		$outTag = '<span class="tag tag-event">Event</span>';
	} elseif ($categoryName == 'Training') {
		$outTag = '<span class="tag tag-event">Course</span>';
	} elseif ($categoryName == 'Jobs') {
		$outTag = '<span class="tag tag-job">Job</span>';
	} elseif ($categoryName == 'Seminar') {
		$outTag = '<span class="tag tag-seminar">Seminar</span>';
	} else {
		$outTag = $categoryName . '<span class="tag tag-general">General</span>';
	}

	return $outTag;
}

// Read more excerpt
function new_excerpt_more( $more ) {
	// Check if course type
	if(get_post_type() == 'course_post') {
		return '<p><a class="read-more" href="'.get_permalink(get_the_ID()).'">'.'More Information'.'</a></p>';
	}

	return ' <a class="read-more" href="' . get_permalink( get_the_ID() ) . '">' . '...more' . '</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );

/*
   * Helpers
   */

// prints a var or array to the screen in a readable way
// Basically echo's a nicer front-end view of an array on the screen
function pr($variable) {
	echo '<pre class="debug-block">Debug: ';
	print_r($variable);
	echo '</pre>';
}

// ONLY USE IF PARAM IS MIXED WITH NAME VARIABLE
function mixed_to_name_string($mixed) {
	$output = '';
	if($mixed != null) {
		$count = 0;
		$len = count($mixed);
		foreach($mixed as $tag) {
			if($count >= $len-1)
				$output .= $tag->name;
			else
				$output .= $tag->name . ', ';
			$count++;
		}
	}

	return $output;
}

// Get twitter generated widget
function get_twitter_timeline_widget($tweetLimit='') {
	?><a class="twitter-timeline" href="https://twitter.com/aus_aust" data-widget-id="628805936795127808" data-tweet-limit=<?php echo $tweetLimit; ?> data-height=200>Tweets by @aus_aust</a>
		<script>!function(d,s,id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';if (!d.getElementById(id)) {js = d.createElement(s);js.id = id;js.src = p + "://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js, fjs);}} (document,"script","twitter-wjs");</script>
<?php
}

//-----------------------------------------------
// Global Options
//-----------------------------------------------
// Option Properties:
// name
// desc
// id
// type
// options
//-----------------------------------------------
$g_options = array();

// Feature journal settings
$g_options[] = array(
	'name' => 'Featured Journal',
	'desc' => 'Select the journal you wish to be featured on the members landing page',
	'id' => 'fj_feature_journal',
	'type' => 'select',
	'options' => get_journals_options()
);

// Get all journals and store in array to be used in drop down menu
function get_journals_options() {
	$args = array(
		'post_type' => 'journals',
		'posts_per_page' => -1,
		'post_status' => 'publish'
	);
	$journals = get_posts($args);
	$results = array();

	// Initial/Default value
	$results[] = array(
		'name' => 'None',
		'value' => '0'
	);

	foreach($journals as $journ) {
		$results[] = array(
			'name' => $journ->post_title,
			'value' => $journ->ID );
	}

	return $results;
}

function ags_options_add_admin() {
	global $g_options;
	if($_GET['page'] == 'AGS_options') {
		if ($_REQUEST['action'] == 'save') {
			foreach ($g_options as $value) {
				update_option( $value['id'], $_REQUEST[ $value['id'] ] );
			}

			// Save Options
			foreach ($g_options as $value) {
				if( isset( $_REQUEST[ $value['id'] ] ) ) {
					update_option( $value['id'], $_REQUEST[ $value['id'] ]  );
				} else {
					delete_option( $value['id'] );
				}
			}

			header("Location: admin.php?page=AGS_options&saved=true");
		} else if ($_REQUEST['action'] == 'reset') {

		}
	}
	// Note: 'edit_themes' capabilities may need further look.
	add_menu_page('AGS Options', 'AGS Options', 'activate_plugins', 'AGS_options', 'draw_global_options');
}


function draw_global_options() {
	global $g_options;
	//pr($_REQUEST);
	//pr($_GET);
	//pr($_SERVER);
	?>
	<div class="wrap">
		<form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="post">
			<h2>AGS Settings</h2>
			<?php if ( $_REQUEST['saved'] ) { ?><div style="clear:both;height:20px;"></div><div class="warning">AGS Options has been updated!</div><?php } ?>
			<?php if ( $_REQUEST['reset'] ) { ?><div style="clear:both;height:20px;"></div><div class="warning">AGS Options has been reset!</div><?php } ?>

			<h3 class="title">General Settings</h3>
			<table class="maintable">
				<?php foreach($g_options as $config): ?>
					<tr class="mainrow">
						<td class="titledesc"><?php echo $config['name']; ?></td>
						<td class="forminp">
							<select name="<?php echo $config['id']; ?>" id="<?php echo $config['id']; ?>">
								<?php foreach($config['options'] as $option): ?>
									<option value="<?php echo $option['value']?>"<?php if(get_option($config['id']) == $option['value']) { echo ' selected'; } ?>><?php echo $option['name']; ?></option>
								<?php endforeach; ?>
							</select>
						</td>
					</tr>
					<tr><span><?php echo $config['desc']; ?></span></tr>
				<?php endforeach; ?>
			</table>

			<p class="submit">
				<input name="save" type="submit" value="Save changes" />
				<input type="hidden" name="action" value="save"/>
			</p>
		</form>
	</div>

<?php }
add_action('admin_menu', 'ags_options_add_admin');
