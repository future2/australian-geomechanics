<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

define('WP_MEMORY_LIMIT', '128M');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'australian_geomechanics');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'f2');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ykW^_pwD56N9Ug[>r^`ed~#@w7);c3xa$|{,+9Qj^g#(3+*#pr0x~/&9vhX%Mm+1');
define('SECURE_AUTH_KEY',  '@&CkN$,x b<-tLE<+lk+dp!6)x<H7f;,.&!jKa+Am[P~W7]UDc[z <m![nH.?yP|');
define('LOGGED_IN_KEY',    ';&@kTDz%bX8nadM%C_*cDrn=f||fx+H A6k!;=L9|#J|!+A $g|5Smh dFa_QgqQ');
define('NONCE_KEY',        '|x$l-A>+x+ ;.m&(a^D6bA9(7{4F3(kZ_IkV#8-#uy#uVvUbb;>iGCDoN-/G!C-L');
define('AUTH_SALT',        'S)h~uSg]O.If_kq}j5;R4H}4/,02%i@mTQ<-FYF#AG(I@*4q_{Z11Md:1rw~2|/d');
define('SECURE_AUTH_SALT', 'b#F(u{GfRm6#)+lM0^8w6.rx>,Nrd7qJ4&i:?|,!.^Vq`f|i/<9>wxHd=BQ[P<H=');
define('LOGGED_IN_SALT',   'Q{ ~v4.;;nVOWVY)WX:L,NEkii|OIbKQaHT~&6wYk%2ToRcG0> NIb{rG:`GJbaS');
define('NONCE_SALT',       ' yetkaQR.Yd(_)C<8M?(b><0C?5+!(ylq-BqI3[tR{pRqtAu$ hXisrC#2->{8vw');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', false);
@ini_set('display_errors', 0);
ini_set('log_errors',TRUE);
ini_set('error_reporting', E_ALL);
ini_set('error_log', dirname(__FILE__) . '/error_log.txt');


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
