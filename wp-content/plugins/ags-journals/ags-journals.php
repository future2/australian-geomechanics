<?php
/*
Plugin Name: AGS Journals
Description: AGS Journals allows the AGS web managers to add PDFs to the site that are accessible by their logged in users.
Version: 1.0
Author: James Heazlewood (Future Medium)
*/
defined('ABSPATH') or die("No script kiddies please!");
add_action( 'init', 'create_journal' );

function create_journal() {
  register_post_type( 'journals',
    array(
      'labels' => array(
        'name' => 'Journals',
        'singular_name' => 'Journal',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Journal',
        'edit' => 'Edit',
        'edit_item' => 'Edit Journal',
        'new_item' => 'New Journal',
        'view' => 'View',
        'view_item' => 'View Journal',
        'search_items' => 'Search Journal',
        'not_found' => 'No Journals found',
        'not_found_in_trash' => 'No Journals found in Trash',
        'parent' => 'Parent Journal'
      ),

      'public' => true,
      'menu_position' => 4,
      'supports' => array('title', 'editor', 'tags'),
      'taxonomies' => array('post_tag'),
      'menu_icon' => 'dashicons-media-document',
      'has_archive' => true
    )
  );
}

//
add_action( 'admin_init', 'add_attachment' );
function add_attachment() {
  add_meta_box(
    'journal_meta_box',
    'Documents',
    'display_journal_meta_boxes',
    'journals',
    'normal'
  );
}

function display_journal_meta_boxes($journal)
{
  $numdocs = 6;
  if(isset($journal->{'number_of_documents'}) && is_numeric($journal->{'number_of_documents'}))
  {
    $numdocs = $journal->{'number_of_documents'};
  }

  for($i = 1; $i <= $numdocs; $i++)
  {
    $attachment = '';
    $pdfViewDisplay = 'none';
    $title = '';

    if(isset($journal->{'attached_pdf_' . $i}))
    {
      $attachment = $journal->{'attached_pdf_' . $i};
      $pdfViewDisplay = 'block';
    }

    if(isset($journal->{'pdf_title_' . $i}))
    {
        $title = $journal->{'pdf_title_' . $i};
    }
        ?>
        <tr>
            <td>
                <h4>Document <?php echo $i; ?>: </h4>
                <label for="journal_pdf_title_<?php echo $i; ?>">Title: </label>
                <input id="journal_pdf_title_<?php echo $i; ?>" type="text" size="36"
                       name="journal_pdf_title_<?php echo $i; ?>" value="<?php echo $title; ?>"/>
                <br/>
                <label for="journal_attached_pdf_<?php echo $i; ?>">PDF file: </label>
                <input id="journal_attached_pdf_<?php echo $i; ?>" type="text" size="36"
                       name="journal_attached_pdf_<?php echo $i; ?>" value="<?php echo $attachment; ?>"/>

                <input id="journal_attached_pdf_button_<?php echo $i; ?>" type="button" value="Choose / upload file"/>
                <span id="journal_attached_pdf_preview_<?php echo $i; ?>"
                      style="display:<?php echo $pdfViewDisplay; ?>;"> - <a target="_blank"
                                                                            href="<?php echo $attachment; ?>">View
                        Attachment</a></span>
                <br/>Enter URL or upload attachment.<br/><br/>
                <hr/>
            </td>
        </tr>

    <?php
  }

    ?>
    <input type="hidden" id="number_of_documents" name="number_of_documents" value="<?php echo $numdocs ?>" />
    <span id="here"></span>
    <input type ="button" class="add" value="Add Document" />
    <?php
}

add_action('admin_print_scripts', 'journal_admin_scripts');
function journal_admin_scripts() {
  wp_register_script('journal-upload', plugin_dir_url( __FILE__ ) . '/media.js', array('jquery','media-upload','thickbox'));
  wp_enqueue_script('journal-upload');
}

add_action('admin_print_styles', 'journal_admin_styles');
function journal_admin_styles() {
  // none at the moment
}
add_action('pre_get_posts', 'before_get_posts');
function before_get_posts($wp_query) {
  if(is_user_logged_in()) {
    //print_r($wp_query->query_vars);

  }
}

// block journals if not logged in
add_filter('pre_get_posts', 'ags_pre_get_posts');
function ags_pre_get_posts($query) {
  if(is_user_logged_in()) {

  } else if(get_post_type() == 'journals') {
    $query->set('post_type', array('post', 'page', 'nav_menu_item', 'revision', 'attachment'));
  }
  return $query;
}


add_action('save_post', 'add_journal_fields', 10, 2);
function add_journal_fields($journal_id, $journal)
{
  // Check post type for movie reviews
    if(isset($_POST['number_of_documents']))
    {
        $numdocs = $_POST['number_of_documents'];
    }
    else
    {
        $numdocs = 6;
    }
  if($journal->post_type == 'journals')
  {
    for($i = 1; $i <= $numdocs; $i++)
    {
      // Store data in post meta table if present in post data
      if(isset($_POST['journal_attached_pdf_' . $i] ) && $_POST['journal_attached_pdf_' . $i] != '' )
      {
        update_post_meta($journal_id, 'attached_pdf_' . $i, $_POST['journal_attached_pdf_' . $i] );
        if(isset($_POST['journal_pdf_title_' . $i] ) && $_POST['journal_pdf_title_' . $i] != '' )
        {
          update_post_meta($journal_id, 'pdf_title_' . $i, $_POST['journal_pdf_title_' . $i] );
        }
        else
        {
          // didn't put a title, rename it to something so we can at least click on it
          update_post_meta($journal_id, 'pdf_title_' . $i, 'Document ' . $i);
        }


      }
    }
      update_post_meta($journal_id, 'number_of_documents', $_POST['number_of_documents'] );
  }

}

function template_chooser($template)
{
  global $wp_query;
  $post_type = get_query_var('post_type');
  if( $wp_query->is_search && $post_type == 'journals' )
  {
    return locate_template('search-journals.php');  //  redirect to archive-search.php
  }
  return $template;
}
add_filter('template_include', 'template_chooser');
?>
