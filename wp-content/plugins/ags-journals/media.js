jQuery(document).ready(function() {
    var count = 6;
  function fieldSelect(id, event, file_frame) {
    event.preventDefault();
    //alert(id);
    // If the media frame already exists, reopen it.
    if(file_frame)
    {
      file_frame.open();
      return;
    }

    // Create the media frame.
    file_frame = wp.media.frames.file_frame = wp.media({
      title: jQuery(this).data('uploader_title'),
      button: {
        text: jQuery(this).data('uploader_button_text')
      },
      multiple: false  // Set to true to allow multiple files to be selected
    });

    // When an image is selected, run a callback.
    file_frame.on('select', function() {
      // We set multiple to false so only get one image from the uploader
      attachment = file_frame.state().get('selection').first().toJSON();
      //alert(id);
      // add to field
      jQuery('#journal_attached_pdf_' + id).val(attachment.url);
      jQuery('#journal_attached_pdf_preview_' + id).show();
      jQuery('#journal_attached_pdf_preview_' + id + ' a').attr('href', attachment.url);
    });

    // Finally, open the modal
    file_frame.open();
  }

  // Uploading files
  var file_frame;
  for(var i = 1; i <= count; i++)
  {
    jQuery('#journal_attached_pdf_button_' + i).live('click', function( event ){
      fieldSelect(jQuery(this).attr('id').replace('journal_attached_pdf_button_', ''), event, file_frame);
    });
  }

  count = parseInt(jQuery("input[id=number_of_documents]").val());
    jQuery(".add").click(function() {
        count = count + 1;

        jQuery('#here').append(
            '<tr>' +
            '<td>' +
            '<h4>Document ' + count + ' </h4>' +
            '<label for="journal_pdf_title_' + count + '">Title: </label>' +
            '<input id="journal_pdf_title_' + count + '" type="text" size="36" name="journal_pdf_title_' + count + '" />' +
            '<br />' +
            '<label for="journal_attached_pdf_' + count + '">PDF file: </label>' +
            '<input id="journal_attached_pdf_' + count + '" type="text" size="36" name="journal_attached_pdf_' + count + '"  />' +

            '<input id="journal_attached_pdf_button_' + count + '" type="button" value="Choose / upload file" />' +
            '<span id="journal_attached_pdf_preview_' + count + '" style="display:' + count + ';"> - <a target="_blank">View Attachment</a></span>' +
            '<br />Enter URL or upload attachment.<br /><br />' +
            '<hr />' +
            '</td>' +
            '</tr>'
        );

        jQuery("input[id=number_of_documents]").val(count);
        jQuery('#journal_attached_pdf_button_' + count).live('click', function( event ){
            fieldSelect(jQuery(this).attr('id').replace('journal_attached_pdf_button_', ''), event, file_frame);
        });
        return false;
    });
});
