<?php
get_header(); ?>
  <h1>Courses</h1>
  <div class="row">
    <article id="post-<?php the_ID(); ?>" <?php post_class('main'); ?>>
      <p>The AGS runs a number of professional training and education courses. These are run according to demand. To register your interest or to inquire further, please contact the course coordinator or secretary@australiangeomechanics.org.</p>
      <?php
      // filter to have wild cards for repeater rows
      function my_posts_where( $where ) {
        $where = str_replace("meta_key = 'course_session_%", "meta_key LIKE 'course_session_%", $where);
        return $where;
      }
      add_filter('posts_where', 'my_posts_where');

      $arg = array(
        'post_type' => 'course_post',
        'order' => 'ASC',
        'orderby' => 'name',
      );
      $query = new WP_Query($arg);

      //if ( have_posts() ) while ( have_posts() ) : the_post();
      if($query->have_posts()) while ($query->have_posts()) : $query->the_post(); ?>
        <h2><?php the_title(); ?></h2>
        <p><?php echo get_the_excerpt(); ?></p>
        <?php // Show date - TODO: Requires formatting
//        if(have_rows('course_session')) {
//          the_row();
//          echo get_sub_field('session_start_date');
//        }
        ?>
      <?php endwhile;
      //pr($query->posts);
      ?>
    </article><!-- #post-## -->
    <?php get_sidebar(); ?>
  </div>
<?php get_footer(); ?>