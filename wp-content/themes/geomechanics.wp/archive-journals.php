<?php
/*
 * The template for displaying Archive pages.
 */

get_header(); ?>

<div id="container">
  <div id="content" class="content-archive" role="main">

    <?php
      /* Queue the first post, that way we know
       * what date we're dealing with (if that is the case).
       *
       * We reset this later so we can run the loop
       * properly with a call to rewind_posts().
       */
      if ( have_posts() )
        the_post();
    ?>
      <div class="breadcrumbs">
        <a href="<?php echo home_url(); ?>/members/">
          User home
          <img src="<?php bloginfo('template_directory'); ?>/_img/icons/arrow-left-red.png" title="back" alt="back arrow" width="20" height="20" />
        </a>
      </div>

      <h1 class="page-title"><?php _e( 'Browse all journals', 'twentyten' ); ?></h1>

      <div class="section-light search-section">
        <div class="search-left">
          <img src="<?php bloginfo('template_directory'); ?>/_img/icons/search.png" width="32" height="32" />
        </div>
        <div class="search-right">
          <form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
            <label for="s" class="assist">Search</label>
            <input type="text" class="field" name="s" id="s" placeholder="Search by topic">
            <input type="hidden" name="post_type" value="journals" />
            <input type="submit" class="submit" name="submit" id="searchsubmit" value="Search">
          </form>
        </div>
      </div>

    <?php
      /* Since we called the_post() above, we need to
       * rewind the loop back to the beginning that way
       * we can run the loop properly, in full.
       */
      rewind_posts();

      /* Run the loop for the archives page to output the posts.
       * If you want to overload this in a child theme then include a file
       * called loop-archives.php and that will be used instead.
       */
       //get_template_part( 'journal-loop', 'archive' );

      ////////////////////////////////
      // the date loop
      $dateLowest = 1971;
      $thisYear = date('Y');
      $yearCount = $thisYear - $dateLowest;

      // draw date box
      function dateBox($startDate, $endDate = 0)
      {
        $limit = 9;
        if(!$endDate == 0) $limit = $startDate - $endDate;
        ?>
        <div class="date-buttons">
        <?php for($i = $startDate; $i >= $startDate - $limit; $i --) { ?>
          <span data-button="<?php echo $i; ?>" class="archive-date-button">
            <?php echo $i; ?>
            <span class="point"></span>
          </span>
        <?php }?>
        </div>
        <div class="accordion-container">
          <?php for($j = $startDate; $j >= $startDate - $limit; $j--) { ?>
            <div data-accordion="<?php echo $j; ?>" class="archive-date-accordion">
              <div class="accordion-header">
                <h2><?php echo $j; ?></h2>
                <span class="close">
                  <img src="<?php bloginfo('template_directory'); ?>/_img/icons/close.png" width="32" height="32" />
                </span>
              </div>
              <?php
                // need to reset the loop every time
                wp_reset_query();
                query_posts('post_type=journals&year=' . $j);
              ?>
              <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); //pr(get_post_meta(get_post()->ID)); ?>
                  <div class="accordion-result">
                    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                      <div class="month">
                        <?php echo get_the_date('F'); ?>
                      </div>
                      <div class="entry">
                        <h2>
                          <a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark">
                            <?php the_title(); ?>
                              <img class="arrow" src="<?php bloginfo('template_directory'); ?>/_img/icons/arrow-right-red.png" width="20" height="20" />
                              <img class="pdf" src="<?php bloginfo('template_directory'); ?>/_img/icons/pdf.png" width="32" height="32" />
                          </a>
                        </h2>

                      </div>
                    </div>
                  </div>
                <?php endwhile; // End the loop. Whew. ?>
              <?php else: ?>
                <div class="accordion-result">
                  <div class="month">&nbsp;</div>
                  <div class="entry">
                    <h2>
                    <a href="#">
                      No journals found in this year
                    </a>
                    </h2>
                  </div>
                </div>
              <?php endif; ?>
            </div>
          <?php } ?>
        </div>
      <?php
      }

      $highestYear = date('Y');
      if($highestYear > 2020) $highestYear = 2020;
    ?>
    <h2><?php echo $highestYear . '-2011'; ?></h2>
    <?php dateBox($highestYear, 2011); ?>
    <h2>2010-2001</h2>
    <?php dateBox(2010); ?>
    <h2>2000-1991</h2>
    <?php dateBox(2000); ?>
    <h2>1990-1981</h2>
    <?php dateBox(1990); ?>
    <h2>1980-1971</h2>
    <?php dateBox(1980); ?>
  </div><!-- #content -->
</div><!-- #container -->

<?php get_footer(); ?>
