<?php
/*
 * The template for displaying Awards Category
 */
get_header();
?>

<h1><?php echo single_cat_title('', false); ?></h1>
<div class="row">
    <div class="main">
        <?php
        // Display the category description if it exists
        $category_description = category_description();
        if (!empty($category_description)) echo '<p>' . $category_description . '</p>';
        ?>

        <?php // If there are no posts to display, such as an empty archive page
        if (!have_posts()) :
            ?>
            <article id="post-0" class="post error404 not-found">
                <h1>Not Found</h1>
                <p>Unfortunately no results were found for the requested archive. Try searching to find a related post.</p>
            <?php get_search_form(); ?>
            </article><!-- #post-0 -->
        <?php endif; ?>

        <ul>
        <?php
        query_posts('cat=12&posts_per_page=-1&orderby=date&order=ASC');
        while (have_posts()) : the_post();
            ?>
            <li><a id="post-<?php the_ID(); ?>" href="index.php?p=<?php the_ID(); ?>"><?php the_title(); ?>
           </a></li>
        <?php endwhile; ?>
        </ul>

        <?php
//      the body of the page.
        query_posts('cat=12&posts_per_page=-1&orderby=date&order=ASC');
        while (have_posts()) : the_post();
            ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class('section-award'); ?>>
                <h1 class="h2 h2-alt"><?php the_title(); ?></h1>
                <div class="entry-content">
                    <?php the_content(); ?>
                </div>
            </article>
        <?php endwhile; ?>
    </div>

    <section class="sidebar">
        <?php
        // Join button
        if (!is_user_logged_in()) {
            ?>
            <div class="link link-member">
                <i class="sprite sprite-join"></i>
                <a href="<?php echo get_permalink(6); ?>">Become a member of the AGS</a>
            </div>
<?php } ?>
        <div class="link link-download">
            <i class="sprite sprite-download"></i>
            <a href="<?php echo get_permalink(87); ?>" title="Downloads and papers">Downloads and papers</a>
        </div>
        <div class="link link-events">
            <i class="sprite sprite-training"></i>
            <a href="<?php echo get_category_link(5); ?>" title="Events">Events</a>
        </div>
        <div class="link link-links">
            <i class="sprite sprite-links"></i>
            <a href="<?php echo get_permalink(96); ?>" title="Related Links">Related Links</a>
        </div>
        <div class="link link-video">
            <i class="sprite sprite-video"></i>
            <a href="<?php echo get_permalink(90); ?>" title="Videos">Videos</a>
        </div>
        <div class="link link-training">
            <i class="sprite sprite-training"></i>
            <a href="<?php echo home_url().'/course_post/'; // This is a temporary fix: echo get_category_link(6); ?>" title="Courses &amp; Training">Courses &amp; Training</a>
        </div>
        <?php get_twitter_timeline_widget(); ?>
<?php get_template_part('loop', 'gallery'); ?>
    </section>
</div>
<?php get_footer(); ?>
