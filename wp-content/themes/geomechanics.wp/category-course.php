<?php
/*
 * The template for displaying Training & Courses Category
 */
get_header(); ?>
<div class="row">
	<h1 class="head-chapter">Courses &amp; Training</h1>

<?php // If there are no posts to display, such as an empty archive page
	if ( ! have_posts() ) : ?>
	<article id="post-0" class="post error404 not-found main">
		<h1>Not Found</h1>
		<p>Unfortunately no results were found for the requested archive. Try searching to find a related post.</p>
		<?php get_search_form(); ?>
	</article><!-- #post-0 -->
<?php endif; ?>

	<div class="main">
	<?php
		query_posts('category_name=course&posts_per_page=10&meta_key=date_start&orderby=meta_value&order=ASC');
		while ( have_posts() ) : the_post();
	?>
		<article id="post-<?php the_ID(); ?>" <?php post_class('list-courses'); ?>>
			<h1 class="h2"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
		</article>
	<?php endwhile; ?>

	<?php /* Display navigation to next/previous pages when applicable */ ?>
	<?php if (  $wp_query->max_num_pages > 1 ) : ?>
		<div id="nav-below" class="navigation">
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'twentyten' ) ); ?></div>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'twentyten' ) ); ?></div>
		</div><!-- #nav-below -->
	<?php endif; ?>
	</div>

	<?php get_sidebar('courses'); ?>

</div>
<?php get_footer(); ?>