<?php
/*
 * The template for displaying Event Category
 */
get_header(); ?>

<h1><?php echo single_cat_title( '', false ); ?></h1>
<?php // Display the category description if it exists
	$category_description = category_description();
	if ( ! empty( $category_description ) )
		echo '<div class="archive-meta">' . $category_description . '</div>';
?>
<div class="row">
	<ul id="filterEvents" class="radio filter">
		<li><strong>Filter: </strong></li>
		<li><label><input type="checkbox" name="event-category" value="australian-events" checked>Australian Events</label></li>
		<li><label><input type="checkbox" name="event-category" value="international-events" checked>International Events</label></li>
		<li><label><input type="checkbox" name="event-category" value="ags-events" checked>AGS Events</label></li>
	</ul>

<?php // If there are no posts to display, such as an empty archive page
	if ( ! have_posts() ) : ?>
	<article id="post-0" class="post error404 not-found">
		<h1>Not Found</h1>
		<p>Unfortunately no results were found for the requested archive. Try searching to find a related post.</p>
		<?php get_search_form(); ?>
	</article><!-- #post-0 -->
<?php endif; ?>

	<div class="span12">
	<?php
		query_posts('cat=5&posts_per_page=-1&meta_key=date_start&orderby=meta_value&order=ASC');
		while ( have_posts() ) : the_post();
	?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<h1 class="h2"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
			<div class="entry-content">
				<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>') ); ?>
				<?php wp_link_pages( array( 'before' => '<div class="page-link">Pages:', 'after' => '</div>' ) ); ?>
			</div>
		</article>
	<?php endwhile; ?>

	<?php /* Display navigation to next/previous pages when applicable */ ?>
	<?php if (  $wp_query->max_num_pages > 1 ) : ?>
		<div id="nav-below" class="navigation">
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'twentyten' ) ); ?></div>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'twentyten' ) ); ?></div>
		</div><!-- #nav-below -->
	<?php endif; ?>
	</div>
</div>
<?php get_footer(); ?>
