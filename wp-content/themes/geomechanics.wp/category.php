<?php
/*
 * The template for displaying Category Archive pages.
 */

get_header();
?>

<section class="main-content span9">
  <h1>
    <?php switch(str_replace(' ', '-', strtolower(single_cat_title('', false)))){
      case 'general':
        echo single_cat_title('', false) . ' Updates';
        $showNews = 'general';
        break;

      case 'queensland':
      case 'victoria':
      case 'western-australia':
      case 'tasmania':
      case 'newcastle':
      case 'south-australia-and-nt':
      case 'sydney':
        echo single_cat_title('', false) . ' Chapter Updates';
        $showNews = str_replace(' ', '-', strtolower(single_cat_title('', false)));
        break;

      default:
        echo single_cat_title( '', false );
        $showNews = false;
        break;
    } ?>
  </h1>
  <?php // Display the category description if it exists
  $category_description = category_description();
  if ( ! empty( $category_description ) )
    echo '<div class="archive-meta">' . $category_description . '</div>';
  ?>

  <?php
  if($showNews){
    $generalCatID = get_cat_ID('general');
    if($showNews == 'general'){
      $generalNews = new WP_Query(
        array(
          'post_type'        => 'post',
          'category__in' => array($generalCatID),
          'meta_query' => array(
            'relation' => 'OR',
            array( //check to see if date has been filled out
              'key' => 'meeting_start',
              'compare' => 'EXISTS'
            ),
            array( //if no date has been added show these posts too
              'key' => 'meeting_start',
              'compare' => 'NOT EXISTS'
            )
          ),
          'order'            => 'DESC',
          'orderby'          => 'meta_value post_date'
        )
      );
      if ($generalNews->have_posts()) {
        while($generalNews->have_posts()) {
          $generalNews->the_post();
          $post = get_post(get_the_ID()); ?>

          <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="detailMeetingContent">
              <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
              <h2><?php echo get_post_meta($post->ID, 'meeting_speaker', true) ?></h2>
              <div class="entry-content">
                <?php the_excerpt(); ?>
              </div>
            </div>
          </article>
        <?php }
      }
      wp_reset_postdata(); ?>
    <?php } else if(!is_user_logged_in()){ ?>
      <p>Please login to see chapter news</p>
    <?php } else { ?>
      <?php
      $chapterNews = new WP_Query(
        array(
          'post_type'        => 'post',
          'category_name'    => $showNews,
          'meta_query' => array(
            'relation' => 'OR',
            array( //check to see if date has been filled out
              'key' => 'meeting_start',
              'compare' => 'EXISTS'
            ),
            array( //if no date has been added show these posts too
              'key' => 'meeting_start',
              'compare' => 'NOT EXISTS'
            )
          ),
          'order'            => 'DESC',
          'orderby'          => 'meta_value post_date'
        )
      );
      if ($chapterNews->have_posts()) {
        while($chapterNews->have_posts()) {
          $chapterNews->the_post();
          $post = get_post(get_the_ID()); ?>

          <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="detailMeetingContent">
              <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
              <h2><?php echo get_post_meta($post->ID, 'meeting_speaker', true) ?></h2>
              <div class="entry-content">
                <?php the_excerpt(); ?>
              </div>
            </div>
          </article>
        <?php }
      }
      wp_reset_postdata(); ?>
    <?php }
  } else {
    get_template_part('loop', 'category');
  }
?>
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>