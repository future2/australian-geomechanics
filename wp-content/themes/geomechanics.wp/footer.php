<?php
/*
 * The template for displaying the footer.
 */
?>
</section>

<footer class="clearfix" role="contentinfo">
    <section id="affiliates">
        <h1>Affiliated Sites</h1>
        <ul>
            <li class="link-isrm"><a href="http://isrm.net/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/_img/logo.isrm.png" alt="International Society for Rock Mechanics" /></a></li>
            <li class="link-iaeg"><a href="http://www.iaeg.info" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/_img/logo.iaeg.png" alt="International Association for Engineering Geology and the Environment" /></a></li>
            <li class="link-issmge"><a href="http://issmge.org" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/_img/logo.issmge.png" alt="International Society for Soil Mechanics and Geotechnical Engineering" /></a></li>
            <li class="link-ea"><a href="<?php echo get_permalink(8360); // 8360 is post id for "Become an ags member" page?>" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/_img/logo.ea.png" alt="Engineers Australia" /></a></li>
            <li class="link-lrm"><a href="http://lrm.australiangeomechanics.org" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/_img/logo.lrm.png" alt="Landslide Risk Management Education Empowerment" /></a></li>
        </ul>
    </section>
    <section id="base">
        <nav>
            <ul>
                <li><a href="<?php echo get_permalink(18); ?>">Contact</a></li>
                <li><a href="https://twitter.com/aus_aust"><span class="icon-twitter-sign"></span></a></li>
                <li><a href="<?php echo get_permalink(16); ?>">Privacy</a></li>
            </ul>
        </nav>
        <p>ABN 89 615 696 393<br />&copy; 2012-<?php echo date('Y') ?> Australian Geomechanics Society</p>
    </section>
</footer>

</div><!-- END CANVAS =========================== -->
<?php //<p id="im" class="container">Website maintained by <a href="http://futuremedium.com.au/" target="_blank">Future Medium</a></p> ?>
<script>window.jQuery || document.write('<script src="<?php bloginfo('template_directory'); ?>/_inc/jquery-1.8.2.min.js"><\/script>')</script>
<script src="<?php bloginfo('template_directory'); ?>/_inc/script.min.js"></script>
<!--
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2XyDirZVK9eWjsGpnNbdPZ4EIo_qh33U&amp;sensor=false"></script>
-->
<script>
    jQuery(document).ready(function($) {
        function initialize() {
            var loc = '<?php echo get_post_meta($post->ID, 'meeting_map', true); ?>';
            var locpoints = loc.slice(loc.indexOf('&ll='), loc.indexOf('&spn=')).slice(4);
            var locpoints = locpoints.split(',');
            var myLatlng = new google.maps.LatLng(locpoints[0], locpoints[1]);
            var mapOptions = {
                center: myLatlng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("mapLocation"), mapOptions);
            var image = '<?php bloginfo('template_directory'); ?>/_img/icons/location.png';
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                icon: image
            });
        }
        if ($('#mapLocation').length > 0) {
            initialize();
        }
    });
</script>
<?php wp_footer(); ?>
</body>
</html>
