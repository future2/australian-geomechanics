`<?php

function ags_setup() {

	// Set timezone to Sydney Australia for date functions
	date_default_timezone_set('Australia/Sydney');


// ++++++++++++++++ REGISTER THEME OBJECTS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //

	// Register menu
	register_nav_menu( 'primary', 'Primary Menu' );


	// Register Post Formats, Custom Thumbnails, HTML5 Comments
	if ( function_exists( 'add_theme_support' ) ):
		add_theme_support( 'post-formats', array( 'aside', 'image', 'link', 'quote', 'status' ) );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form' ) );
	endif;


	// Register image sizes
	if ( function_exists( 'add_image_size' ) ):
		add_image_size( 'gallery-home', 460, 350, false );
		add_image_size( 'gallery-side', 320, 400, false );
	endif;


// ++++++++++++++++ PERFORMANCE +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //

	// Enable GZIP compression
	if(extension_loaded("zlib") && (ini_get("output_handler") != "ob_gzhandler"))
	add_action('wp', create_function('', '@ob_end_clean();@ini_set("zlib.output_compression", 1);'));


	// Remove the version query string from scripts and styles - allows for better caching
	function im_remove_script_version( $src ){
		$parts = explode( '?', $src );
		return $parts[0];
	}
	add_filter( 'script_loader_src', 'im_remove_script_version', 15, 1 );
	add_filter( 'style_loader_src', 'im_remove_script_version', 15, 1 );


	// Prevents WordPress from testing SSL capability on domain.com/xmlrpc.php?rsd when XMLRPC not in use
	remove_filter('atom_service_url','atom_service_url_filter');


// ++++++++++++++++ SECURITY MODIFICATIONS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //

	// Hide login form error messages
	add_filter('login_errors',create_function('$a', "return 'Error';"));


	// Remove WordPress version number
	function im_remove_wp_version() { return ''; }
	add_filter('the_generator', 'im_remove_wp_version');


	// Disable XMLRPC
	add_filter('xmlrpc_enabled', '__return_false');


// ++++++++++++++++ WP CLEANUP ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //

	// General Cleanup - removes unnecessary WordPress features
	remove_action( 'wp_head', 'feed_links_extra', 3 );
	remove_action( 'wp_head', 'feed_links', 2 );
	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'index_rel_link' );
	remove_action( 'wp_head', 'parent_post_rel_link' );
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	remove_action( 'wp_head', 'wp_generator' );
	remove_action( 'wp_head', 'wp_shortlink_wp_head');


	// Unregister all default WP Widgets
	function im_unregister_default_wp_widgets() {
		unregister_widget('WP_Widget_Pages');
		unregister_widget('WP_Widget_Calendar');
		unregister_widget('WP_Widget_Archives');
		unregister_widget('WP_Widget_Links');
		unregister_widget('WP_Widget_Meta');
		unregister_widget('WP_Widget_Search');
		unregister_widget('WP_Widget_Text');
		unregister_widget('WP_Widget_Categories');
		unregister_widget('WP_Widget_Recent_Posts');
		unregister_widget('WP_Widget_Recent_Comments');
		unregister_widget('WP_Widget_RSS');
		unregister_widget('WP_Widget_Tag_Cloud');
	}
	add_action( 'widgets_init', 'im_unregister_default_wp_widgets', 1 );


	// Remove inline style from Recent Comments widget
	function im_remove_recent_comments_style() {
		global $wp_widget_factory;
		remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
	}
	add_action( 'widgets_init', 'im_remove_recent_comments_style' );


	// Set the maximum number of post revisions unless the constant is already set in wp-config.php
	if (!defined('WP_POST_REVISIONS')) define('WP_POST_REVISIONS', 2);


	// Disable Auto-Formatting in Content and Excerpt
	remove_filter( 'the_content', 'wpautop' );
	remove_filter( 'the_excerpt', 'wpautop' );


	// Disable Auto Linking of URLs in comments
	remove_filter('comment_text', 'make_clickable', 9);


	// Disable self-ping
	function im_self_ping( &$links ) {
		$home = get_option( 'home' );
		foreach ( $links as $l => $link )
			if ( 0 === strpos( $link, $home ) )
				unset($links[$l]);
	}
	add_action( 'pre_ping', 'im_self_ping' );


// ++++++++++++++++ BASIC HELPER FUNCTIONS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //

	// Adds MIME types for uploading
	function addUploadMimes($mimes) {
		$mimes = array_merge($mimes, array(
			'epub|mobi' => 'application/octet-stream'
		));
		return $mimes;
	}
	add_filter('upload_mimes', 'addUploadMimes');


	// Gets the ID of a page based on a slug
	if ( ! function_exists( 'get_id_by_slug' ) ) :
		function get_id_by_slug($page_slug) {
			$page = get_page_by_path($page_slug);
			if ($page) {
				$page_id = $page->ID;
				return $page_id;
			} else {
				return null;
			}
		}
	endif;


	// Checks for subpage and returns ID of parent
	function is_subpage() {
		global $post;
		if ( is_page() && $post->post_parent ) {
			return $post->post_parent;
		} else {
			return false;
		}
	}


	// Returns parent slug
	function the_parent_slug() {
		global $post;
		if($post->post_parent == 0) return '';
		$post_data = get_post($post->post_parent);
		return $post_data->post_name;
	}


	// Add category slugs in body and post class
	function category_id_class($classes) {
		global $post;
		foreach((get_the_category($post->ID)) as $category)
			$classes[] = $category->category_nicename;
		return $classes;
	}
	add_filter('post_class', 'category_id_class');
	add_filter('body_class', 'category_id_class');


// ++++++++++++++++ ADMIN MODIFICATIONS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //

	// Change the default WordPress greeting in Admin
	function im_replace_howdy( $wp_admin_bar ) {
		$my_account=$wp_admin_bar->get_node('my-account');
		$newtitle = str_replace( 'Howdy,', 'Welcome, ', $my_account->title );
		$wp_admin_bar->add_node( array(
			'id' => 'my-account',
			'title' => $newtitle,
		) );
	}
	add_filter( 'admin_bar_menu', 'im_replace_howdy', 25 );


// ++++++++++++++++ LOGIN MODIFICATIONS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //

	// Custom login logo
	function im_custom_login_logo() {
		echo '<style type="text/css">
			.login #login { padding-top: 0;}
			.login h1 a { background:url(' . get_template_directory_uri() . '/_img/ags.png) 50% 50% no-repeat; background-size: 50%; height: 200px; width: 320px; }
			</style>';
	}
	add_action('login_head', 'im_custom_login_logo');


	// Custom login URL
	function im_custom_login_url(){
		return ('http://australiangeomechanics.org/');
	}
	add_filter('login_headerurl', 'im_custom_login_url');


	// Custom login URL Title Attribute
	function im_custom_login_title(){
		return ('Australian Geomechanics Society');
	}
	add_filter('login_headertitle', 'im_custom_login_title');


// ++++++++++++++++ USER PROFILE MODIFICATIONS ++++++++++++++++++++++++++++++++++++++++++++++++++++ //

	// Add extra profile fields
	function ags_profile_fields( $user ) { ?>

		<h3>Member Information</h3>

		<table class="form-table">
			<tr>
				<th>Chapter</th>
				<td>
					<ul class="radio">
						<li><label><input type="radio" <?php if (get_the_author_meta( 'chapter', $user->ID) == 'Queensland' ) { ?>checked="checked"<?php }?> name="chapter" value="Queensland"> Queensland</label></li>
						<li><label><input type="radio" <?php if (get_the_author_meta( 'chapter', $user->ID) == 'Newcastle' ) { ?>checked="checked"<?php }?> name="chapter" value="Newcastle"> Newcastle, NSW</label></li>
						<li><label><input type="radio" <?php if (get_the_author_meta( 'chapter', $user->ID) == 'Sydney' ) { ?>checked="checked"<?php }?> name="chapter" value="Sydney"> Sydney, NSW</label></li>
						<li><label><input type="radio" <?php if (get_the_author_meta( 'chapter', $user->ID) == 'Victoria' ) { ?>checked="checked"<?php }?> name="chapter" value="Victoria"> Victoria</label></li>
						<li><label><input type="radio" <?php if (get_the_author_meta( 'chapter', $user->ID) == 'Tasmania' ) { ?>checked="checked"<?php }?> name="chapter" value="Tasmania"> Tasmania</label></li>
						<li><label><input type="radio" <?php if (get_the_author_meta( 'chapter', $user->ID) == 'South Australia' ) { ?>checked="checked"<?php }?> name="chapter" value="South Australia"> South Australia &amp; NT</label></li>
						<li><label><input type="radio" <?php if (get_the_author_meta( 'chapter', $user->ID) == 'Western Australia' ) { ?>checked="checked"<?php }?> name="chapter" value="Western Australia"> Western Australia</label></li>
						<li><label><input type="radio" <?php if (get_the_author_meta( 'chapter', $user->ID) == 'Overseas' ) { ?>checked="checked"<?php }?> name="chapter" value="Overseas"> Overseas</label></li>
					</ul>
				</td>
			</tr>
			<tr>
				<th><label for="company">Company</label></th>
				<td>
					<input type="text" name="company" id="company" value="<?php echo esc_attr( get_the_author_meta( 'company', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description">Please enter your employer name leave blank if you are a student</span>
				</td>
			</tr>
			<tr>
				<th><label for="address">Address</label></th>
				<td>
					<textarea name="address" id="address" class="regular-text"><?php echo esc_attr( get_the_author_meta( 'address', $user->ID ) ); ?></textarea><br />
					<span class="description">Please enter your postal address</span>
				</td>
			</tr>
		</table>
	<?php }
	add_action( 'show_user_profile', 'ags_profile_fields' );
	add_action( 'edit_user_profile', 'ags_profile_fields' );

	// Add extra profile fields
	function ags_save_profile_fields( $user_id ) {
		if ( !current_user_can( 'edit_user', $user_id ) )
			return false;

		update_usermeta( $user_id, 'chapter', $_POST['chapter'] );
		update_usermeta( $user_id, 'company', $_POST['company'] );
		update_usermeta( $user_id, 'address', $_POST['address'] );
	}
	add_action( 'personal_options_update', 'ags_save_profile_fields' );
	add_action( 'edit_user_profile_update', 'ags_save_profile_fields' );


// ++++++++++++++++ NON-ADMIN USER PROFILE MODIFICATIONS ++++++++++++++++++++++++++++++++++++++++++ //

	/* Profile options for non-Administrators ======================== */
	if (!current_user_can('manage_options')) {

		// Remove WP button
		function remove_admin_bar_links() {
			global $wp_admin_bar;
			$wp_admin_bar->remove_menu('wp-logo');
		}
		add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );

		// Remove Dashboard from menu
		function remove_menus () {
			global $menu;
			$restricted = array(__('Dashboard'));
			end ($menu);
			while (prev($menu)){
				$value = explode(' ',$menu[key($menu)][0]);
				if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
			}
		}
		add_action('admin_menu', 'remove_menus');

		// Remove colour, visual editor, keyboard shortcuts, and toolbar options
		remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );
		if ( ! function_exists( 'cor_remove_personal_options' ) ) {
		  function cor_remove_personal_options( $subject ) {
			$subject = preg_replace( '#<h3>Personal Options</h3>.+?/table>#s', '', $subject, 1 );
			return $subject;
		  }
		  function cor_profile_subject_start() {
			ob_start( 'cor_remove_personal_options' );
		  }
		  function cor_profile_subject_end() {
			ob_end_flush();
		  }
		}
		add_action( 'admin_head-profile.php', 'cor_profile_subject_start' );
		add_action( 'admin_footer-profile.php', 'cor_profile_subject_end' );

		// Hide Admin Bar from users on site
		add_filter('show_admin_bar', '__return_false');

		// Remove profile fields
		function remove_contactmethod( $contactmethods ) {
			unset($contactmethods['url']);
			unset($contactmethods['aim']);
			unset($contactmethods['jabber']);
			unset($contactmethods['yim']);
			$contactmethods['phone'] = 'Phone number';
			return $contactmethods;
		}
		add_filter('user_contactmethods','remove_contactmethod',10,1);

		add_filter( 'contextual_help', 'ags_remove_help', 999, 3 );
		function ags_remove_help($old_help, $screen_id, $screen){
			$screen->remove_help_tabs();
			return $old_help;
		}

	}

}

add_action( 'after_setup_theme', 'ags_setup' );


// prevent pagination on archive
function no_nopaging($query) {
  if(is_post_type_archive()) {
    $query->set('nopaging', 1);
  }
}
add_action('parse_query', 'no_nopaging');


?>