<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<title><?php

	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( 'Page %s' ), max( $paged, $page );

	?></title>
	<meta name="description" content="The website of the Australian Geomechanics Society">
	<meta name="google-site-verification" content="v_5qNkFIKjk8CFMwcbpXt2fWFzNTAVkrYFjzibYCmto" />
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<?php	wp_head() ?>

	<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>?<?php echo filemtime(get_stylesheet_directory()) ?>">
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/style-extra.css?<?php echo filemtime(get_stylesheet_directory()) ?>">
	<!--[if lt IE 8]><link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/_inc/ie.css"><![endif]-->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory'); ?>/_inc/modernizr-2.5.3-min.js"></script>
	<link rel="shortcut icon" href="/favicon.ico">
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-64756510-1', 'auto');
		ga('send', 'pageview');

	</script>
</head>
<body <?php body_class(); ?>>

<section id="search" class="section-search<?php if(!is_user_logged_in()) { echo ' not-logged-in'; } ?>" role="search">
  <div class="nav-box-inner search-inner">
    <?php get_search_form(); ?>
  </div>
  <div class="nav-box-inner account-inner">
    <div class="container">
      <?php
      // Show this content to public users
      if(is_user_logged_in()) {
        global $current_user;
        get_currentuserinfo();
        ?>
        <p class="user-welcome">
					Welcome, <strong><?php echo ucfirst($current_user->display_name); ?></strong> of the <strong><?php echo $current_user->chapter; ?> Chapter</strong>
				</p>
        <ul class="user-controls">
          <li><a href="<?php echo home_url(); ?>">User home</a></li>
          <li><a href="/user-account/">Edit profile</a></li>
					<?php // Need to find a new way to get permalinks - because staging server cannot handle post name permalinks?>
<!--			<li><a href="--><?php //echo get_bloginfo('url').'/chapters/'.$current_user->chapter; ?><!--">Committee Home</a></li>-->
					<?php
						// DO NOT USE THIS
						// TODO: Replace this
						// This is a temp fix, until the staging permalink issue is resolved
						$user_chapter = $current_user->chapter;
						$chapterName = '';
						if ( $user_chapter == 'Queensland' ) {
							$chapterName =  'queensland';
						} elseif ( $user_chapter == 'Newcastle' ) {
							$chapterName =  'newcastle';
						} elseif ( $user_chapter == 'Sydney' ) {
							$chapterName =  'sydney';
						} elseif ( $user_chapter == 'Victoria' ) {
							$chapterName =  'victoria';
						} elseif ( $user_chapter == 'Tasmania' ) {
							$chapterName =  'tasmania';
						} elseif ( $user_chapter == 'South Australia' ) {
							$chapterName =  'south-australia-and-nt';
						} elseif ( $user_chapter == 'Western Australia' ) {
							$chapterName =  'western-australia';
						}
						$pageInfo = get_page_by_path('chapters/'.$chapterName, OBJECT, 'page');
						$resourcePageInfo = get_page_by_path('chapters/'.$chapterName.'/resources/')
					?>
					<li><a href="<?php echo get_permalink($pageInfo->ID); ?>">Committee Home</a></li>
					<?php
					if(get_user_meta($current_user->ID, 'committee-member', true)){ ?>
						<li><a href="<?php echo get_permalink($resourcePageInfo->ID); ?>">Resources</a></li>
					<?php } ?>
<!--	Check user level Here -->
          <li class="right"><a href="<?php echo wp_logout_url(home_url()); ?>">Logout</a></li>
        </ul>
        <?php
      } else {
        echo '<p>Please log in to see account options.</p>'; // Only seen in html source
      }
      ?>
    </div>
  </div>
</section>

<style>
a.logoright{float:right;margin:58px 0 0 0; height:auto; width: auto}
.container{width:1020px; margin:0 auto}
</style>

<div id="canvas" class="container">

	<div class="head clearfix">
		<header class="clearfix" role="banner">
<a class="logoright"> </a>
	<a class="logoright"><img src="http://australiangeomechanics.org/admin/wp-content/uploads/2017/07/Engineers-Australia.png" width ="135" height="155" alt="Engineers Australia"></a>

			<a class="logo" href="<?php bloginfo('url');?>"><img src="<?php bloginfo('template_directory'); ?>/_img/ags.png" width="155" height="138" alt="Australian Geomechanics Society"></a>
			<div class="masthead">
				<h1><a href="<?php bloginfo('url');?>">Australian Geomechanics Society <i class="icon icon-home"></i></a></h1>
				<a class="assist" href="#main" title="Skip to content">Skip to Content</a>
				<nav id="mobile-nav" role="navigation" class="mobile-nav">
					<button id="mobile-activate">Menu</button>
					<?php
					if(is_user_logged_in() ){
						wp_nav_menu( array( 'container_class' => 'menu-header', 'menu' => 'Main menu','theme_location' => 'primary' ) );
					} else {
						wp_nav_menu( array( 'container_class' => 'menu-header', 'menu' => 'Main-Menu-Logged-Out', 'theme_location' => 'primary' ) );
					}
					?>
				</nav>
			</div>
		</header>

		<nav class="nav-buttons">
			<ul>
				<?php // <li class="btn-promote"><a href="/icsmge2017/">ICSMGE 2017 Bid</a></li> ?>
				<li class="btn-search"><a href="#search" rel="search">Search</a></li>
				<?php // Show this content to public users
					if ( !is_user_logged_in() ) {
				?>
				<li class="btn-login"><a href="<?php echo get_permalink(8360); // 8360 is post id for "Become an ags member" page?>">Join</a></li>
				<li class="btn-login"><a href="<?php echo wp_login_url(home_url()); ?>">Login</a></li>
				<?php } else { // show this to logged in user ?>

        <li class="btn-login btn-account"><a href="#account">Account</a></li>

        <?php } ?>
			</ul>

		</nav>

	</div>

	<section id="main">