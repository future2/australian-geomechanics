<?php
/**
 * The main template file.
 *
 * @package WordPress
 * @subpackage Australian Geomechanics Society
 * @since Australian Geomechanics Society 2.0
 */

get_header(); ?>

<section class="row">
	<div class="span8">
		<?php get_template_part( 'loop', 'index' ); ?>
	</div>
	<div class="span4">
		<?php get_sidebar(); ?>
	</div>
</section>

<?php get_footer(); ?>
