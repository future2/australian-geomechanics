<?php
/*
 * The loop that displays posts.
 */
?>

<?php /* If there are no posts to display, such as an empty archive page */ ?>
<?php if ( ! have_posts() ) : ?>
	<article id="post-0" class="post error404 not-found">
		<h1 class="entry-title">Not Found</h1>
		<div class="entry-content">
			<p>Apologies, but no results were found for the requested archive. Perhaps searching will help find a related journal.</p>
		</div>
	</article>
<?php endif; ?>
<?php while ( have_posts() ) : the_post(); ?>
  <div class="section-light search-result">
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<h2 class="entry-title">
        <img class="pdf" src="<?php bloginfo('template_directory'); ?>/_img/icons/pdf.png" width="22" height="22" />
        <a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark">
          <?php the_title(); ?>
        </a>
      </h2>
      <p>Found in journal:</p>
      <h3><a href="<?php the_permalink(); ?>">
          <?php the_title(); ?>
          <img class="pdf" src="<?php bloginfo('template_directory'); ?>/_img/icons/arrow-right-red.png" width="20" height="20" />
        </a></h3>

      <p><?php the_date('F Y'); ?>&nbsp;</p>
    </div>
  </div>
<?php endwhile; // End the loop. Whew. ?>

<?php /* Display navigation to next/previous pages when applicable */ ?>
<?php if (  $wp_query->max_num_pages > 1 ) : ?>
    <div id="nav-below" class="navigation">
      <div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Next', 'twentyten' ) ); ?></div>
      <div class="nav-next"><?php previous_posts_link( __( 'Prev <span class="meta-nav">&rarr;</span>', 'twentyten' ) ); ?></div>
    </div><!-- #nav-below -->
<?php endif; ?>
