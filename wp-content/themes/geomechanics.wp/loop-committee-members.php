<?php
/**
 * Created by PhpStorm.
 * User: ronaldchau
 * Date: 7/07/15
 * Time: 2:32 PM
 * Template Name: Committee Members
 */
?>
<?php
// For some reason - $current_user in the header is not recognised here
global $current_user;
get_currentuserinfo();
$chapterslug = $post->post_name;
?>

<div class="row">
<?php
  $chapterPath = 'chapters/'.$chapterslug;
  echo '<h1 class="head-alt"><a href="' . get_page_link(get_page_by_path($chapterPath)->ID) . '"><i class="icon-arrow-left"></i>';
  $chapterSearch = '';
  if ( $chapterslug == 'queensland' ) {
    echo 'Queensland';
    $chapterSearch = 'Queensland';
  } elseif ( $chapterslug == 'newcastle' ) {
    echo 'Newcastle';
    $chapterSearch = 'Newcastle';
  } elseif ( $chapterslug == 'sydney' ) {
    echo 'Sydney';
    $chapterSearch = 'Sydney';
  } elseif ( $chapterslug == 'victoria' ) {
    echo 'Victoria';
    $chapterSearch = 'Victoria';
  } elseif ( $chapterslug == 'tasmania' ) {
    echo 'Tasmania';
    $chapterSearch = 'Tasmania';
  } elseif ( $chapterslug == 'south-australia-and-nt' ) {
    echo 'South Australia &amp; NT';
    $chapterSearch = 'South Australia';
  } elseif ( $chapterslug == 'western-australia' ) {
    echo 'Western Australia';
    $chapterSearch = 'Western Australia';
  } elseif ( $chapterslug == 'national-committee') {
    echo 'National Committee';
    $chapterSearch = 'National Committee';
  }
  echo ' Chapter</a></h1>';
?>

 <article class="span-full">
<h1 class="page-title">Committee</h1>
   <!--
<table class="committee committee-chapter">
  <thead>
    <tr>
      <?php if(get_field('show_location') == true):?>
        <td class="">Location</td>
      <?php endif; ?>
      <td class="">Position</td>
      <td class="">Name</td>
      <td class="">Email</td>
      <?php if(get_field('show_phone') == true):?>
        <td class="">Phone</td>
      <?php endif; ?>
    </tr>
  </thead>
  <tbody>
  <?php if(have_rows('committee_member')): while(have_rows('committee_member')): the_row();?>
    <tr>
      <?php if(get_field('show_location') == true):?>
        <td class=""><?php the_sub_field('committee_location'); ?></td>
      <?php endif; ?>
      <td class=""><?php the_sub_field('committee_position'); ?></td>
      <td class=""><?php the_sub_field('committee_name'); ?></td>
      <td class=""><a href="mailto:<?php the_sub_field('committee_email'); ?>"><?php the_sub_field('committee_email'); ?></a></td>
      <?php if(get_field('show_phone') == true):?>
      <td class=""><?php the_sub_field('committee_phone'); ?></td>
      <?php endif; ?>
    </tr>
  <?php endwhile; endif; ?>
  </tbody>
</table>
-->
  <?php
  // Query users by chapter meta data
  if($chapterslug != 'national-committee'){
    $userQuery = array(
      'meta_key' => 'committee-position',
      'orderby' => 'menu_order',
      'order' => 'ASC',
      'meta_query' => array(
        'relation' => 'AND',
        array(
          'key' => 'chapter',
          'value' => $chapterSearch,
          'compare' => '='
        ),
        array(
          'key' => 'committee-member',
          'value' => 'committee-member',
          'compare' => '='
        )
      ),
    );
  } else {
    $userQuery = array(
      'meta_key' => 'committee-position',
      'orderby' => 'menu_order',
      'order' => 'ASC',
      'meta_query' => array(
        array(
          'key' => 'national-committee-member',
          'value' => 'national-committee-member',
          'compare' => '='
        )
      )
    );
  }

  // returns all things in $data that match
  // $committeeName in the committee key
  // ----------------------------------------
  // order of member list:
  // Chair
  // Deputy or Vice Chair
  // Secretary
  // Treasurer
  // Media Officer
  // Publicity
  // National Committee Elected Representative
  // Committee Member
  function addCommitteePosition($array, $committeeName) {
    $n = array();
    foreach($array as $d) {
      if(isset($d['committee'])) {
        if($d['committee'] == '') $d['committee'] = 'Committee Member';
        if($committeeName != false) {
          if(strtolower($d['committee']) == strtolower($committeeName)) $n[] = $d;
        } else {
          if(strtolower($d['committee']) !== strtolower('Chair') &&
              strtolower($d['committee']) !== strtolower('Deputy') &&
              strtolower($d['committee']) !== strtolower('Deputy Chair') &&
              strtolower($d['committee']) !== strtolower('Vice Chair') &&
              strtolower($d['committee']) !== strtolower('Secretary') &&
              strtolower($d['committee']) !== strtolower('Treasurer') &&
              strtolower($d['committee']) !== strtolower('Media Officer') &&
              strtolower($d['committee']) !== strtolower('Publicity') &&
              strtolower($d['committee']) !== strtolower('National Committee Elected Representative') &&
              strtolower($d['committee']) !== strtolower('Committee Member')
          ) $n[] = $d;
        }
      }

    }
    return $n;
  }

  $users = get_users($userQuery);
  $usersArray = array();
  foreach($users as $user) {
    $id = $user->ID;
    $usersArray[] = array(
        'chapter' => get_user_meta($id, 'chapter', true),
        'committee' => get_user_meta($id, 'committee-position', true),
        'email' => $user->user_email,
        'name' => $user->display_name,
        'phone' => get_user_meta($id, 'member-phone', true)
    );
  }

  // Chair
  // Deputy or Vice Chair
  // Secretary
  // Treasurer
  // Media Officer
  // Publicity
  // National Committee Elected Representative
  // Committee Member
  $sortedUsers = array();
  echo '<!--';
  print_r($usersArray);
  echo '-->';
  $sortedUsers = array_merge($sortedUsers, addCommitteePosition($usersArray, 'Chair'));
  $sortedUsers = array_merge($sortedUsers, addCommitteePosition($usersArray, 'Vice Chair'));
  $sortedUsers = array_merge($sortedUsers, addCommitteePosition($usersArray, 'Deputy'));
  $sortedUsers = array_merge($sortedUsers, addCommitteePosition($usersArray, 'Deputy Chair'));
  $sortedUsers = array_merge($sortedUsers, addCommitteePosition($usersArray, 'Secretary'));
  $sortedUsers = array_merge($sortedUsers, addCommitteePosition($usersArray, 'Treasurer'));
  $sortedUsers = array_merge($sortedUsers, addCommitteePosition($usersArray, 'Media Officer'));
  $sortedUsers = array_merge($sortedUsers, addCommitteePosition($usersArray, 'Publicity'));
  $sortedUsers = array_merge($sortedUsers, addCommitteePosition($usersArray, 'National Committee Elected Representative'));
  $sortedUsers = array_merge($sortedUsers, addCommitteePosition($usersArray, false));
  $sortedUsers = array_merge($sortedUsers, addCommitteePosition($usersArray, 'Committee Member'));
  echo '<!--';
  print_r($sortedUsers);
  echo '-->';

  //usort($users, 'cmp');
  ?>
  <!--
  <table class="committee committee-chapter">
    <thead>
    <tr>
      <?php if(get_field('show_location') == true):?>
        <td>Location</td>
      <?php endif; ?>
      <td>Position</td>
      <td>Name</td>
      <?php if(is_user_logged_in()):?>
        <td>Email</td>
      <?php endif; ?>
      <?php if(get_field('show_phone') == true && is_user_logged_in()):?>
        <td>Phone</td>
      <?php endif; ?>
    </tr>
    </thead>
     <tbody>
        <?php if(isset($users)): foreach($users as $usr): ?>
        <tr>
          <?php if(get_field('show_location') == true): ?>
            <td><?php echo get_user_meta($usr->ID, 'chapter', true); ?></td>
          <?php endif; ?>
          <td><?php echo get_user_meta($usr->ID, 'committee-position', true); ?></td>
          <td><?php echo $usr->display_name; ?></td>
          <?php if(is_user_logged_in()):?>
            <td><a href="mailto:<?php echo $usr->user_email; ?>"><?php echo $usr->user_email; ?></a></td>
          <?php endif; ?>
          <?php if(get_field('show_phone') == true): ?>
            <td class=""><?php echo get_user_meta($usr->ID, 'member-phone', true); ?></td>
          <?php endif; ?>
        </tr>
        <?php endforeach; endif; wp_reset_query(); ?>
      </tbody>
    </table>
   -->
   <table class="committee committee-chapter">
     <thead>
     <tr>
       <?php if(get_field('show_location') == true):?>
         <td>Location</td>
       <?php endif; ?>
       <td>Position</td>
       <td>Name</td>
       <?php if(is_user_logged_in()):?>
         <td>Email</td>
       <?php endif; ?>
       <?php if(get_field('show_phone') == true && is_user_logged_in()):?>
         <td>Phone</td>
       <?php endif; ?>
     </tr>
     </thead>
     <tbody>
     <?php if(isset($sortedUsers)): foreach($sortedUsers as $usr): ?>
       <tr>
         <?php
         /*
          *         'chapter' => get_user_meta($id, 'chapter', true),
        'committee' => get_user_meta($id, 'committee-position', true),
        'email' => $user->user_email,
        'name' => $user->display_name,
        'phone' => get_user_meta($id, 'member-phone', true)
          */
         ?>
         <?php if(get_field('show_location') == true): ?>
           <td><?php echo $usr['chapter']; ?></td>
         <?php endif; ?>
         <td><?php echo $usr['committee']; ?></td>
         <td><?php echo $usr['name']; ?></td>
         <?php if(is_user_logged_in()):?>
           <td><a href="mailto:<?php echo $usr['email']; ?>"><?php echo $usr['email']; ?></a></td>
         <?php endif; ?>
         <?php if(get_field('show_phone') == true): ?>
           <td><?php echo $usr['phone']; ?></td>
         <?php endif; ?>
       </tr>
     <?php endforeach; endif; wp_reset_query(); ?>
     </tbody>
   </table>
  </article>
</div>
<?php get_footer(); ?>
