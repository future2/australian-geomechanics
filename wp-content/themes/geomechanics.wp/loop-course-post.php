<?php
get_header();

if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
  <h1><?php the_title(); ?></h1>
  <div class="row">
    <section class="span8">
      <div class="box box-dash">
        <div id="more-info" class="box-promo box-content">
          <div id="content-show"><?php the_content(); ?></div>
          <div id="more-info-btn" class="link-hover link-arrow-right-red">
            <!--TODO: Need to make a standard icon location code-->
            <h2 class="h2-black-alt">MORE INFORMATION</h2>
          </div>
          <div id="content-hidden"></div>
        </div>
      </div>

      <h1>Register for the course:</h1>
      <?php if(is_user_logged_in()) : ?>
        <div id="regis-member-submit" class="btn-form-submit">Apply for course</div>
        <div id="regis-edit-detail" class="btn-form-submit">Edit my details</div>
      <?php endif;
      $nameField = $current_user->first_name.' '.$current_user->last_name;
      $emailField = $current_user->user_email;
      ?>
      <div class="course-register-form <?php if(!is_user_logged_in()) : echo 'active'; endif; ?>">
        <form id="course-regis-form" action="MAILTO:<?php echo get_field('course_submission_email');?>?subject=<?php the_title();?>" enctype="text/plain" method="post">
          <input type="hidden" id="regis-name-field" name="name" value="">
          <input type="hidden" id="regis-email-field" name="email" value="">
          <input type="hidden" id="regis-phone-field" name="phone" value="">
          <input type="hidden" id="regis-course-field" name="course" value="<?php the_title(); ?>">
        </form>
        <div class="course-register-field">
          <h2 class="h2-black-alt">NAME: </h2><input type="text" id="regis-name" value="<?php echo $nameField; ?>">
        </div>
        <div class="course-register-field">
          <h2 class="h2-black-alt">EMAIL: </h2><input type="text" id="regis-email" value="<?php echo $emailField; ?>">
        </div>
        <div class="course-register-field">
          <h2 class="h2-black-alt">PHONE: </h2><input type="text" id="regis-phone" value="">
        </div>
        <?php if(!is_user_logged_in()) : ?>
          <div id="regis-submit" class="btn-form-submit course-register-btn">Submit</div>
        <?php endif; ?>
      </div>
    </section>
    <section class="span4">
      <div class="box box-meeting-alt">
        <h3 class="h2-black-alt">When &amp; Where</h3>
        <?php if(have_rows('course_session')): while(have_rows('course_session')) : the_row(); ?>
          <p><?php
            $format = 'g:ia';
            $dateDisp = '';
            // Date
            $startDate = DateTime::createFromFormat('dmY', get_sub_field('session_start_date'));
            $endDate = DateTime::createFromFormat('dmY', get_sub_field('session_end_date'));

            $dateDisp .= $startDate->format('M jS');
            if($endDate != null)
            {
              $dateDisp .= ' to '.$endDate->format('M jS');
            }
            $dateDisp .= ', ';

            // Time
            $startTime = DateTime::createFromFormat($format, get_sub_field('session_start_time'));
            $endTime = DateTime::createFromFormat($format, get_sub_field('session_end_time'));

            $dateDisp .= $startTime->format($format);
            if($endTime != null)
              $dateDisp .= ' to '.$endTime->format($format);
            echo $dateDisp;
            ?><br>
            <b><?php the_sub_field('session_venue_name'); ?></b><br>
            <?php the_sub_field('session_venue_address'); ?><br>
            <?php if(get_sub_field('session_registration_form') != ''):?>
              <a href="<?php the_sub_field('session_registration_form');?>">Registration Form</a>
            <?php endif; ?></p>
        <?php endwhile; endif; ?>
        <h3 class="h2-black-alt">Numbers</h3>
        <p><?php echo the_field('course_numbers'); ?></p>
        <h3 class="h2-black-alt">Enquires</h3>
        <p>For further information contact:</p>
        <?php if(have_rows('course_enquires')): while(have_rows('course_enquires')) : the_row(); ?>
          <?php the_sub_field('contact_name'); ?><br>
          <?php the_sub_field('contact_email'); ?><br><br>
        <?php endwhile; endif; ?>
      </div>
    </section>
  </div>
<?php endwhile; ?>

<?php get_footer(); ?>