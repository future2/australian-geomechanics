<?php
/*
 * The loop that displays training and courses
 */
?>

<h1 class="head-single">Courses &amp; Training</h1>
<div class="row single-entry">

<?php
	if ( have_posts() ) while ( have_posts() ) : the_post();
?>
	<article id="post-<?php the_ID(); ?>" <?php post_class('main'); ?>>
		<h1 class="h2"><?php the_title(); ?></h1>
		<div class="entry-content">
			<?php the_content(); ?>
		</div>
	</article>

<?php
	endwhile;
?>

	<section class="sidebar">
		<?php // Join button
			if ( !is_user_logged_in() ) {
		?>
		<div class="link link-member">
			<i class="sprite sprite-join"></i>
			<a href="<?php echo get_permalink(6); ?>">Become a member of the AGS</a>
		</div>
		<?php } ?>

		<?php get_template_part( 'loop', 'gallery' ); ?>

	</section>
</div>