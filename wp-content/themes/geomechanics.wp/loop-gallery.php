<?php

if (function_exists('slideshow')) {
  $gallery_override = get_field('gallery_override');
  if(!$gallery_override){
    $gallery_id = get_option('fg_feature_gallery');
    if(!$gallery_id){
      $gallery_id = 1;
    }
  } else {
    $gallery_id = $gallery_override;
  }
  slideshow(true, $gallery_id, false, array(
    //'layout' => 'responsive',
    'width' => "220",
    'height' => "220"
  ));
}