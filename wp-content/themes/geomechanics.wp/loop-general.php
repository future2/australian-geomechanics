<?php
/**
 * The Template for displaying all general posts.
 */
?>

<h1><?php the_title(); ?></h1>
<div class="row single-entry">
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class('main'); ?>>
		<?php if (in_category('news')) { ?>
			<p class="timestamp"><time><?php echo the_date(); ?></time></p>
		<?php } ?>
		<div class="entry-content">
			<?php the_content(); ?>
		</div>
	</article>
<?php endwhile; ?>

	<section class="sidebar">
		<?php if ( !is_user_logged_in() ) { ?>
		<div class="link link-member">
			<i class="sprite sprite-join"></i>
			<a href="<?php echo get_permalink(6); ?>">Become a member of the AGS</a>
		</div>
		<?php } ?>
		<?php get_template_part( 'loop', 'gallery' ); ?>
	</section>
</div>