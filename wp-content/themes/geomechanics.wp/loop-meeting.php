<?php
/**
 * The Template for displaying all meeting posts.
 */
?>

<?php
if (have_posts()) while (have_posts()) : the_post();
  ?>
  <div class="row">
    <h1 class="head-alt">
      <?php
      if (in_category('queensland')) {
        $pageid = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = 'queensland'");
        echo '<a href="' . get_permalink($pageid) . '"><i class="icon-arrow-left"></i>';
        echo 'Queensland';
      } elseif (in_category('newcastle')) {
        $pageid = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = 'newcastle'");
        echo '<a href="' . get_permalink($pageid) . '"><i class="icon-arrow-left"></i>';
        echo 'Newcastle';
      } elseif (in_category('sydney')) {
        $pageid = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = 'sydney'");
        echo '<a href="' . get_permalink($pageid) . '"><i class="icon-arrow-left"></i>';
        echo 'Sydney';
      } elseif (in_category('victoria')) {
        $pageid = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = 'victoria'");
        echo '<a href="' . get_permalink($pageid) . '"><i class="icon-arrow-left"></i>';
        echo 'Victoria';
      } elseif (in_category('tasmania')) {
        $pageid = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = 'tasmania'");
        echo '<a href="' . get_permalink($pageid) . '"><i class="icon-arrow-left"></i>';
        echo 'Tasmania';
      } elseif (in_category('south-australia-and-nt')) {
        $pageid = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = 'south-australia-and-nt'");
        echo '<a href="' . get_permalink($pageid) . '"><i class="icon-arrow-left"></i>';
        echo 'South Australia &amp; NT';
      } elseif (in_category('western-australia')) {
        $pageid = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = 'western-australia'");
        echo '<a href="' . get_permalink($pageid) . '"><i class="icon-arrow-left"></i>';
        echo 'Western Australia';
      }
      echo ' Chapter</a>';
      ?>
    </h1>

    <div class="main">
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="hgroup">
          <h1><?php the_title(); ?></h1>
          <?php
          if (get_post_meta($post->ID, 'meeting_speaker', true))
            echo '<h2>' . get_post_meta($post->ID, 'meeting_speaker', true) . '</h2>';
          ?>
          <?php
          if (get_post_meta($post->ID, 'meeting_subtitle', true))
            echo '<h3>' . get_post_meta($post->ID, 'meeting_subtitle', true) . '</h3>';
          ?>
        </div>
        <div class="entry-content">
          <?php the_content(); ?>
          <?php
          if (get_post_meta($post->ID, 'meeting_note', true))
            echo '<div class="meeting-note"><h2 class="h3">Note</h2>' . get_post_meta($post->ID, 'meeting_note', true) . '</div>';
          ?>
          <p class="cpd">Engineers Australia members participating in AGS technical sessions can record attendance on
            their personal CPD logs. Members should refer to Engineers Australia CPD policy for details on CPD types,
            requirements and auditing guidelines.</p>
        </div>
      </article>
    </div>

    <section class="sidebar">
      <?php // Checks for the existence of downloads
      if (get_post_meta($post->ID, 'meeting_paper', true) OR get_post_meta($post->ID, 'meeting_presentation', true) OR get_post_meta($post->ID, 'meeting_video', true)) {

        /*if ( is_user_logged_in() ) {*/
        ?>
        <div class="box box-meeting meeting-downloads">
          <h2>Resources</h2>
          <ul>
            <?php
            if (get_post_meta($post->ID, 'meeting_presentation', true)) {
              echo '<li><a href="' . get_post_meta($post->ID, 'meeting_presentation', true) . '" target="_blank">Presentation</a></li>';
            }
            if (get_post_meta($post->ID, 'meeting_paper', true)) {
              echo '<li><a href="' . get_post_meta($post->ID, 'meeting_paper', true) . '" target="_blank">Meeting Paper</a></li>';
            }
            if (get_post_meta($post->ID, 'meeting_video', true)) {
              echo '<li><a href="' . get_post_meta($post->ID, 'meeting_video', true) . '" target="_blank">Meeting Video</a></li>';
            }
            ?>
          </ul>
        </div>
        <?php /*
      } else {
  ?>
      <div class="box box-meeting meeting-downloads">
        <h2>Resources</h2>
        <p class="meeting-login-error">Please log in via the tab at the top of this page to access downloads.</p>
      </div>
  <?php
      }
    */ ?>
      <?php
      }
      ?>

      <?php // Checks for the existence of the Meeting Flyer
      if (get_post_meta($post->ID, 'meeting_file', true)) { ?>
        <div class="box box-meeting meeting-flyer">
          <p><a href="<?php echo get_post_meta($post->ID, 'meeting_file', true); ?>" target="_blank">Meeting Flyer</a>
          </p>
        </div>
      <?php } ?>

      <?php // Date functions
      $rawdate = get_post_meta($post->ID, 'meeting_start', true);
      $y = substr($rawdate, 0, 4);
      $m = substr($rawdate, 4, 2);
      $d = substr($rawdate, 6, 2);
      $rawdate = $d . '-' . $m . '-' . $y;
      $eventDay = substr(date("l", strtotime($rawdate)), 0, 3);
      $eventDate = date("d M Y", strtotime($rawdate));
      ?>
      <div class="box box-meeting meeting-date">
        <span><?php echo $eventDay . '&nbsp;' . $eventDate; ?></span>
      </div>

      <?php // Checks for the existence of the Meeting Time
      if (get_post_meta($post->ID, 'meeting_time', true)) { ?>
        <div class="box box-meeting meeting-time">
          <span><?php echo get_post_meta($post->ID, 'meeting_time', true); ?></span>
        </div>
      <?php } ?>

      <?php // Checks for the existence of the Meeting Cost
      if (get_post_meta($post->ID, 'meeting_cost', true)) { ?>
        <div class="box box-meeting meeting-cost">
          <span><?php echo get_post_meta($post->ID, 'meeting_cost', true); ?></span>
        </div>
      <?php } ?>

      <?php // Checks for the existence of the RSVP
      if (get_post_meta($post->ID, 'meeting_rsvp', true)) { ?>
        <div class="box box-meeting meeting-rsvp">
          <span>RSVP</span>

          <p><?php echo get_post_meta($post->ID, 'meeting_rsvp', true); ?></p>
        </div>
      <?php } ?>

      <?php // Checks for the existence of the Location
      if (get_post_meta($post->ID, 'meeting_location', true)) { ?>
        <div class="box box-meeting meeting-location">
          <p><?php echo get_post_meta($post->ID, 'meeting_location', true); ?></p>
        </div>
      <?php } ?>

      <?php // Checks for a map
      if (get_post_meta($post->ID, 'meeting_map', true)) {
        echo '<div id="mapLocation"></div><p class="map-disclaimer">Please check event details for meeting venues.</p>';
      }
      ?>

    </section>
  </div>
<?php endwhile; ?>