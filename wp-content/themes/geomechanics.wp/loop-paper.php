<?php
/**
 * The Template for displaying all journal papers.
 */
?>

<?php
	if ( have_posts() ) while ( have_posts() ) : the_post();
?>

<div class="row">
	<hgroup class="journal">
		<h1>Australian Geomechanics Journal</h1>
		<h2>
		<?php
			if (get_post_meta($post->ID, 'paper_volume', true)) {
				echo 'Volume ' . get_post_meta($post->ID, 'paper_volume', true) . ' ';
			}
			if (get_post_meta($post->ID, 'paper_issue', true)) {
				echo ', Issue ' . get_post_meta($post->ID, 'paper_issue', true) . ' ';
			}
			if (get_post_meta($post->ID, 'paper_pubdate', true)) {
				echo ', ' . get_post_meta($post->ID, 'paper_pubdate', true) . '.';
			}
			if (get_post_meta($post->ID, 'paper_issuetitle', true)) {
				echo '<em>' . get_post_meta($post->ID, 'paper_issuetitle', true) . '</em>';
			}
		?>
		</h2>
	</hgroup>

	<div class="main">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<hgroup>
				<h1><?php the_title(); ?></h1>
				<?php
					if (get_post_meta($post->ID, 'paper_author', true)) {
						echo '<h2>';
						$l = count(get_post_meta($post->ID, 'paper_author', false));
						$i = 1;
						if ($l > 1) {
							$authors = get_post_meta($post->ID, 'paper_author', false);
							foreach ($authors as $author) {
								if ($i < ($l-1)) {
									echo $author . ', ';
								} elseif ($i < $l) {
									echo $author . ' & ';
								} else {
									echo $author;
								}
								$i++;
							}
						} else {
							echo get_post_meta($post->ID, 'paper_author', true);
						}
						echo '</h2>';
					}
				?>
			</hgroup>
			<div class="entry-content">
				<h2 class="h3">Abstract</h2>
				<?php the_content(); ?>
			</div>

			<?php if( has_tag() ) { ?>
			<div class="entry-keywords">
				<h2 class="h3">Keywords</h2>
				<?php the_tags('<ul class="paper-keywords"><li>','</li><li>','</li></ul>'); ?>
			</div>
			<?php } ?>
		</article>
	</div>

	<section class="sidebar">
		<div class="box box-downloads">
			<h1>Download</h1>
		<?php
			// SHOW LOGIN PROMPT FOR UNKNOWN USERS
			if ( !is_user_logged_in() ) { ?>
			<p><a href="<?php echo wp_login_url(home_url()); ?>">Login</a> to read and download papers from the Australian Geomechanics Journal.</p>
		</div>

		<?php get_template_part( 'loop', 'gallery' ); ?>
		
		<?php } ?>

		<?php
			// SHOW DOWNLOAD LINKS ONLY TO LOGGED IN USERS
			if ( is_user_logged_in() ) { ?>
			<ul class="download-paper">
			<?php
				// PDF Download Link
				if (get_post_meta($post->ID, 'paper_pdf', true)) {
					echo '<li><a class="download-paper-pdf" href="' . get_post_meta($post->ID, 'paper_pdf', true) . '"><i class="sprite sprite-download"></i>PDF</a></li>';
				}
				// MOBI Download Link
				if (get_post_meta($post->ID, 'paper_mobi', true)) {
					echo '<li><a class="download-paper-mobi" href="' . get_post_meta($post->ID, 'paper_mobi', true) . '"><i class="sprite sprite-download"></i>MOBI</a></li>';
				}
				// ePUB Download Link
				if (get_post_meta($post->ID, 'paper_epub', true)) {
					echo '<li><a class="download-paper-epub" href="' . get_post_meta($post->ID, 'paper_epub', true) . '"><i class="sprite sprite-download"></i>ePUB</a></li>';
				}
			?>
			</ul>
		</div>

		<div class="box box-reference">
			<ul>
				<?php if (get_post_meta($post->ID, 'paper_pdf', true))
					echo '<li><acronym title="Portable Document Format">PDF</acronym> &ndash; an all purpose document format for reading online, offline and on mobile devices. You may need to install a PDF browser plugin or reader, such as <a href="http://www.adobe.com/reader/">Adobe Reader</a> to view these files.</li>';
				?>
				<?php if (get_post_meta($post->ID, 'paper_mobi', true))
					echo '<li><abbr title="Mobipocket">MOBI</abbr> &ndash; an eBook format for use with Amazon Kindle devices and applications.</li>';
				?>
				<?php if (get_post_meta($post->ID, 'paper_epub', true))
					echo '<li><abbr title="electronic publication">ePUB</abbr> &ndash; an eBook format compatible with Apple iBooks, Kobo, Adobe Digital Editions, and Google Books. Suitable for iOS and Android tablet devices.</li>';
				?>
			</ul>
		</div>
		<?php } ?>
	</section>

</div>

<?php
	endwhile;
?>