<?php
/**
 * Created by PhpStorm.
 * User: ronaldchau
 * Date: 14/07/15
 * Time: 2:23 PM
 */
?>

<?php while(have_posts()) : the_post(); ?>
<h1><?php the_title(); ?></h1>
<div class="row">
  <section class="span9">
    <div class="section-light box-resources content">
      <?php the_content(); ?>
    </div>
  </section>
  <section class="span3">
    <?php if(get_field('resource_file_attachment')): ?>
    <div class="section-light box-resources">
      <h2>Downloads</h2>
      <a href="<?php echo get_field('resource_file_attachment'); ?>">
        <div class="centered">
          <img src="<?php echo get_bloginfo('template_directory'); ?>/_img/icons/pdf.png"><?php echo get_field('resource_file_name'); ?>
        </div>
      </a>
    </div>
    <?php endif; ?>
    <?php if(get_field('resource_link_attachment')): ?>
    <div class="section-light box-resources">
      <h2>Links</h2>
      <a href="<?php echo get_field('resource_link_attachment'); ?>">
        <div class="centered">
          <img src="<?php echo get_bloginfo('template_directory'); ?>/_img/icons/links.png"><?php echo get_field('resource_link_name'); ?>
        </div>
      </a>
    </div>
    <?php endif; ?>
  </section>
</div>
<?php endwhile; ?>
