<?php
/*
  Template Name: Members Landing Page
 */
get_header();
?>

<?php
// Show this content to public users
//if (!is_user_logged_in()) {
    ?>
    <p></p>
    <div class="row">
        <article class="type-page span12">
            <div class="box-promo">
            <?php if (have_posts()) while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
            <?php
            endwhile;
            wp_reset_query();
            wp_reset_postdata();
            ?>
            </div>
        </article>
    </div>
<?php //} ?>


<?php
query_posts('cat=112&showposts=5');
if (have_posts()) {
    while (have_posts()) {
        the_post();
        ?>
        <div class="box-promo">
            <?php the_content(); ?>
            <br>
        </div>
    <?php
    } // end while loop
} // end if
wp_reset_query();
?>
<?php
// Show this content to loged in users only
if(is_user_logged_in()) { ?>
    <div class="row">
        <article class="type-page span12">
            <h1>Journal</h1>
            <?php
            query_posts(array(
                'post_type' => 'journals',
                'showposts' => 1
            ));
            ?>
            <?php while (have_posts()) : the_post(); ?>
                <div class="section-light">
                    <?php
                    $meta_values  = get_post_meta(get_the_ID(), '', true);
                    $tags = get_the_tags();
                    ?>
                    <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
                    <p class="journal-date"><?php echo get_the_date('F Y'); ?></p>
                    <p><?php echo get_the_excerpt(); ?></p>
                </div>
                <div class="issues">
                    <div class="accordion-header">
                        <h2>In this issue</h2>
                    </div>
                    <?php
                    if (isset($meta_values['number_of_documents'][0]))
                    {
                        $number_of_journals = $meta_values['number_of_documents'][0];
                    }
                    else
                    {
                        $number_of_journals = 6;
                    }

                    for($i = 1; $i <= $number_of_journals; $i++) { ?>
                        <?php if(isset($meta_values['attached_pdf_' . $i][0])) { ?>
                            <div class="accordion-result">
                                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                    <div class="month">
                                        <?php echo $i ?>
                                    </div>
                                    <div class="entry">
                                        <h2>
                                            <a href="<?php echo $meta_values['attached_pdf_' . $i][0]; ?>" title="<?php echo $meta_values['pdf_title_' . $i][0]; ?>">
                                                <?php echo $meta_values['pdf_title_' . $i][0]; ?>
                                                <img class="arrow" src="<?php bloginfo('template_directory'); ?>/_img/icons/arrow-right-red.png" width="20" height="20" />
                                                <img class="pdf" src="<?php bloginfo('template_directory'); ?>/_img/icons/pdf.png" width="32" height="32" />
                                            </a>
                                        </h2>

                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="download-box">
                    <div class="left">
                        <a href="/articles/archive">
                            <img src="<?php bloginfo('template_directory'); ?>/_img/icons/history.png" width="32" height="32" />
                        </a>
                    </div>
                    <div class="journal-link">
                        <a href="<?php echo get_post_type_archive_link('journals'); ?>">Browse all journal issues </a>
                    </div>
                </div>
            <?php endwhile;?>
        </article>
    </div>
<?php
}
?>
<div class="row">

    <section class="span9">
        <h1>Events &amp; News</h1>
        <div class="box box-updates">
            <h1>Events Diary</h1>
            <table>
                <?php
                $args = array('posts_per_page' => '10', 'order' => 'DSC');
                //                query_posts($args);
                //                query_posts('&meta_key=meeting_start&orderby=meta_value');
                //                $querystr = "
                //                    SELECT * FROM $wpdb->posts
                //                    LEFT JOIN $wpdb->postmeta ON($wpdb->posts.ID = $wpdb->postmeta.post_id)
                //                    WHERE $wpdb->postmeta.meta_key = 'tdo_date'
                //                    ORDER BY $wpdb->postmeta.meta_value ASC limit 10";
                $querystr = "
                    SELECT p.ID, p.post_author, STR_TO_DATE(m.meta_value,'%Y%m%d') AS rec_date, p.post_title, p.guid,
                    t.name, coalesce(STR_TO_DATE(m.meta_value,'%Y%m%d'), p.post_date) AS sort_date
                    FROM wp_posts p
                    LEFT JOIN wp_postmeta m ON (p.ID = m.post_id AND m.meta_key = 'meeting_start')
                    LEFT JOIN wp_term_relationships ON (p.ID = wp_term_relationships.object_id)
                    LEFT JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id)
                    LEFT JOIN wp_terms t ON (wp_term_taxonomy.term_id = t.term_id)

                    WHERE p.post_status = 'publish'
                    AND p.post_type = 'post'
                    AND wp_term_taxonomy.parent = 0
                    AND t.term_id IN (16,111)
                    AND STR_TO_DATE(m.meta_value,'%Y%m%d') > STR_TO_DATE(now(),'%Y-%m-%d')

                    ORDER BY sort_date asc ";

                $pageposts = $wpdb->get_results($querystr, OBJECT);

                if ($pageposts) {
                    foreach ($pageposts as $post) {
                        setup_postdata($post);
                        echo "<tr>";
                        echo "<td class='tagcell'>";
                        if ($post->name == 'Meeting') {
                            echo '<span class="tag tag-meeting">Meeting</span>';
                        } elseif ($post->name == 'Video') {
                            echo '<span class="tag tag-video">Video</span>';
                        } elseif ($post->name == 'Download') {
                            echo '<span class="tag tag-download">Download</span>';
                        } elseif ($post->name == 'News') {
                            echo '<span class="tag tag-news">News</span>';
                        } elseif ($post->name == 'Event') {
                            echo '<span class="tag tag-event">Event</span>';
                        } elseif ($post->name == 'Training') {
                            echo '<span class="tag tag-event">Course</span>';
                        } elseif ($post->name == 'Courses') {
                            echo '<span class="tag tag-event">Course</span>';
                        } elseif ($post->name == 'Jobs') {
                            echo '<span class="tag tag-job">Job</span>';
                        } else {
                            echo $post->name . '<span class="tag tag-general">General</span>';
                        }
                        echo "</td>";
                        echo "<td class='datecell'>";
                        $postDate = new DateTime($post->sort_date);
                        echo $postDate->format('d/m/Y');
                        echo "</td>";
                        echo "<td><a href='";
                        if (in_category('jobs')) {
                            echo get_permalink(27);
                        } else {
                            echo get_permalink($post->ID);
                        }
                        echo "'</a>";
                        if (strlen($post->post_title) > 65) {
                            echo substr(get_the_title($before = '', $after = '', FALSE), 0, 65) . '...';
                        } else {
                            the_title();
                        }
                        echo "</td></tr>";
                    }
                }
                ?>
            </table>
        </div>

        <div class="box box-updates">
            <h1>Recent updates</h1>
            <table>
                <?php
                $args = array('posts_per_page' => '10', 'order' => 'DSC');
                //                query_posts($args);
                //                query_posts('&meta_key=meeting_start&orderby=meta_value');
                //                $querystr = "
                //                    SELECT * FROM $wpdb->posts
                //                    LEFT JOIN $wpdb->postmeta ON($wpdb->posts.ID = $wpdb->postmeta.post_id)
                //                    WHERE $wpdb->postmeta.meta_key = 'tdo_date'
                //                    ORDER BY $wpdb->postmeta.meta_value ASC limit 10";
                $querystr = "
                    SELECT p.ID, p.post_author, p.post_date, p.post_title, p.guid, t.name,
                    coalesce(STR_TO_DATE(m.meta_value,'%Y%m%d'), p.post_date) AS sort_date
                    FROM wp_posts p
                    LEFT JOIN wp_postmeta m ON (p.ID = m.post_id AND m.meta_key = 'meeting_start')
                    LEFT JOIN wp_term_relationships ON (p.ID = wp_term_relationships.object_id)
                    LEFT JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id)
                    LEFT JOIN wp_terms t ON (wp_term_taxonomy.term_id = t.term_id)
                    WHERE p.post_status = 'publish'
                    AND p.post_type = 'post'
                    AND wp_term_taxonomy.parent = 0
                    AND t.term_id IN (1.5,9,11,14)
                    AND p.post_date > DATE_SUB(NOW(), INTERVAL 3 MONTH)

                    ORDER BY sort_date asc ";

                $pageposts = $wpdb->get_results($querystr, OBJECT);

                if ($pageposts) {
                    foreach ($pageposts as $post) {
                        setup_postdata($post);
                        echo "<tr>";
                        echo "<td class='tagcell'>";
                        if ($post->name == 'Meeting') {
                            echo '<span class="tag tag-meeting">Meeting</span>';
                        } elseif ($post->name == 'Video') {
                            echo '<span class="tag tag-video">Video</span>';
                        } elseif ($post->name == 'Download') {
                            echo '<span class="tag tag-download">Download</span>';
                        } elseif ($post->name == 'News') {
                            echo '<span class="tag tag-news">News</span>';
                        } elseif ($post->name == 'Event') {
                            echo '<span class="tag tag-event">Event</span>';
                        } elseif ($post->name == 'Training') {
                            echo '<span class="tag tag-event">Course</span>';
                        } elseif ($post->name == 'Jobs') {
                            echo '<span class="tag tag-job">Job</span>';
                        } else {
                            echo $post->name . '<span class="tag tag-general">General</span>';
                        }
                        echo "</td>";
                        echo "<td class='datecell'>";
                        $postDate = new \DateTime($post->sort_date);
//                        echo $postDate->format('d/m/Y');
                        echo "</td>";
                        echo "<td><a href='";
                        if (in_category('jobs')) {
                            echo get_permalink(27);
                        } else {
                            echo the_permalink();
                        }
                        echo "'</a>";
                        if (strlen($post->post_title) > 65) {
                            echo substr(the_title($before = '', $after = '', FALSE), 0, 65) . '...';
                        } else {
                            the_title();
                        }
                        echo "</td></tr>";
                    }
                }
                ?>
            </table>
        </div>

        <div class="box-promo">
            <h2>AGS not deterred – will bid again to bring the International Conference to Sydney in 2021</h2>
            <div class="box-promo">
                <a href="http://australiangeomechanics.org/wp-content/uploads/2015/02/2021-Flyer-2014.pdf" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/_img/icsmge2021.jpg" alt="ICSMGE 2021 - A Geotechnical Discovery Down Under"></a>
                <p>The Australian Geomechanics Society will again bid to host the International Conference on Soil Mechanics and Geotechnical Engineering. Having narrowly missed out on the 2017 conference to South Korea we are confident that our experience of the bid process will place us in a strong position to bring the quadrennial conference to Sydney in 2021.</p>
                <p>The conference brings significant technical and commercial benefits. The 2013 conference in Paris attracted over 1,600 delegates including many of the world’s leading geotechnical practitioners. We would love our members to benefit from this experience in Australia.</p>
                <p>Our bid was officially  launched at the International Conference on Environmental Geotechnics in Melbourne in November 2014. The host city will be decided at the 2017 conference.</p>
                <p>If you would like to help on our bid team please <strong>contact Graham Scholey, gscholey@golder.com.au. </strong>We need people with lobbying skills, logistics expertise and people who know influential people in the geotechnical world globally!</p></p>
            </div>
        </div>

    </section>
    <div class="span3">

        <style type="text/css">
            .bgimg {
                background-image: url(<?php bloginfo('template_directory'); ?>/_img/course.png);
            }
        </style>



        <?php
        if (is_user_logged_in()) {
            echo '<div class="box box-about">
                    <p>About the Australian Geomechanics Society</p>
                    <a href="' . get_permalink(6) . '" class="btn btn-right">Read more</a>
                </div>';
        } else {
            echo '<div class="box box-join">
                    <p>Become a member of the Australian Geomechanics Society</p>
                    <a href="' . get_permalink(4) . '" class="btn btn-right">Join now</a>
                </div>';
        }
        ?>

        <div class="box box-subscribe">
            <h1>Subscribe</h1>
            <p>Keep informed about events, jobs and AGS meetings. Subscribe for updates via email or <abbr title="Really Simple Syndication">RSS</abbr>.</p>
            <a href="<?php echo get_permalink(1210); ?>" class="btn btn-right">Subscribe</a>
        </div>

    </div>
</div>

<?php get_footer(); ?>
