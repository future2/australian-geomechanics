<?php
/**
 * Created by PhpStorm.
 * User: ronaldchau
 * Date: 15/07/15
 * Time: 1:29 AM
 * Template Name: Add Resource
 */
?>
<?php get_header();
  /**TODO
   * To add resources
   * Must use wp_insert_post to add the post data
   * The hard part is to upload files
   * Upload file to WP and then reference the file to ACF
   *
   */
  // Check if user is logged
  // Will not check for the same chapter as the add resources is not chapter specific and add resources to the chapter the user is in.
if(!is_user_logged_in()):
  echo '<div class="row page-header"><section class="span14">';
  echo '<h1>You must be logged in or be in the same chapter to access this resources</h1>';
  echo '<p>Not a member? <a href="'. get_permalink(6609) .'">Join Us</a></p>';
  echo '</section></div>'; ?>
  <?php else: ?>
  <div class="row page-header">
    <section class="span14">
      <div class="breadcrumbs">
        <a class="pointer" onClick="history.go(-1)">Return to Resources
          <img src="<?php bloginfo('template_directory'); ?>/_img/icons/arrow-left-red.png" title="back" alt="back arrow" width="20" height="20" />
        </a>
      </div>
      <h1>Add a Resource</h1>
    </section>
  </div>
  <div class="row">
    <form id="submit-resources" action="<?php get_permalink(); ?>" method="post" enctype="multipart/form-data">
      <input type="hidden" name="postChapter" value="<?php echo $current_user->chapter; ?>">
      <div class="resource-input-field">
        <h3>TITLE</h3>
        <i class="input-warning"></i>
        <input id="submit-resource-title" name="postTitle" type="text">
      </div>
      <div class="resource-input-field">
        <h3>DESCRIPTION</h3>
        <i class="input-warning"></i>
        <textarea id="submit-resource-content" name="postContent"></textarea>
      </div>
      <div class="resource-input-field">
        <h3>TAGS</h3>
        <i class="input-warning"></i>
        <input id="submit-resource-tag" name="postTags" type="text" placeholder="Examples: road, earth moving, equipment, chapter event, lectures, etc">
        <div class="input-sub-script">
          <i>Tags should be short and contain two or three words at most. They are meant to represent important details contained within the text so they should always be relevant to the post content.</i>
        </div>
      </div>
      <div class="resource-input-field">
        <h3>UPLOAD FILE</h3>
        <input id="submit-resource-file" name="resourceAttachment" type="file" class="btn-upload" />
        <i class="input-warning">Upload files cannot exceed 20mb in size</i>
      </div>
      <div class="resource-input-field">
        <input value="Add Resource" type="submit" class="btn-add-resource">
      </div>
    </form>
  </div>
<?php

  // Has the form been submitted
  // Validates form after submit button is pressed
  if($_SERVER[REQUEST_METHOD] == 'POST') {
    // if true, the post is good and will be sent to the server
    // if false, the post is not ok and will not be sent to the server
    $is_post_ok = true;

    pr($_FILES);
    pr($_POST);

    // Dependency for media_handle_upload
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    require_once(ABSPATH . 'wp-admin/includes/file.php');
    require_once(ABSPATH . 'wp-admin/includes/media.php');

    // Check if there are any error in files
    if($_FILES['resourceAttachment']['error'] != UPLOAD_ERR_OK &&
        $_FILES['resourceAttachment']['error'] != UPLOAD_ERR_NO_FILE) {
      $is_post_ok = false;
    }

    // Validate Post Data
    // Check if post data is not empty
    if (isset($_POST)) {
      if($_POST['error'] != UPLOAD_ERR_OK) {
        $is_post_ok = false;
      }

      if($is_post_ok == true) {
        // Attach input into insert_post
        $resourcePost = array(
          'post_title' => $_POST['postTitle'],
          'post_content' => $_POST['postContent'],
          'post_type' => 'resources',
          'post_status' => 'publish',
          'tags_input' => $_POST['postTags'],
          'tax_input' => array(
            'chapter' => $_POST['postChapter']
          )
        );
        $post_id = wp_insert_post($resourcePost);
        if (isset($post_id)) {

          // param is the name attribute for input type file
          // 0 == media attachment is not attach to post
          // return post id for attachment
          $attachment_id = media_handle_upload('resourceAttachment', 0);

          // Find field key for file upload
          update_field(field_55a4a2de28cd6, $_FILES['resourceAttachment']['name'], $post_id); // resource_file_name
          update_field(field_55a48acde7672, $attachment_id, $post_id);  // resource_file_attachment

          // Redirect page to new post
          wp_redirect(get_permalink($post_id));
          exit;
        }
      }
    }
  }
  endif;
?>

<?php get_footer();?>