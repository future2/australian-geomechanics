<?php
/**
 * Created by PhpStorm.
 * User: ronaldchau
 * Date: 13/08/15
 * Time: 3:13 PM
 *
 * Template Name: AGS Journal
 *
 * Desc: Used for the featured AGS Journal Page
 */


get_header();
?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<h1><?php the_title(); ?></h1>
<div class="row">
  <article id="post-<?php the_ID(); ?>" class="span-full">
    <?php the_content(); ?>
  </article>
  <?php endwhile; ?>
</div>

<?php
if(is_user_logged_in()) { ?>
  <h1>Latest Issue</h1>
  <div class="row">
    <article class="type-page span-full">
      <?php
      global $post;
      $journal_id = get_option('fj_feature_journal');
      $post = get_post($journal_id);
      setup_postdata($post); ?>
      <?php if(isset($post)): ?>
        <div class="section-light journal-header">
          <?php
          $meta_values  = get_post_meta($journal_id, '', true);
          $tags = get_the_tags();
          ?>
          <div class="alignleft">
            <?php if(get_field('journal_cover_image')): ?>
              <img src="<?php echo get_field('journal_cover_image'); ?>" />
            <?php endif; ?>
          </div>
          <div class="alignleft content-excerpt">
            <h2><a href="<?php echo get_permalink($journal_id); ?>"><?php echo get_the_title(); ?></a></h2>
            <?php if(get_field('journal_release_date')) {
              echo '<p class="journal-date">';
              // in YYmmdd format
              $releaseDate = DateTime::createFromFormat('Ymd', get_field('journal_release_date'));
              echo $releaseDate->format('F Y');
              echo '</p>';
            } ?>
            <p><?php the_content(); ?></p>
          </div>
        </div>
        <?php $journalFile = get_field('journal_file', $journal_id);
        if($journalFile): ?>
          <div class="issues">
            <div class="accordion-header"><h2>Download</h2></div>
            <table class="in-this-issue">
              <tbody>
              <div class="accordion-result l-padding-40">
                <div class="entry">
                  <h2><a href="<?php echo $journalFile ?>" title="<?php echo get_field('journal_title', $journal_id); ?>">
                      <?php echo get_field('journal_title', $journal_id); ?>
                      <img class="arrow" src="<?php bloginfo('template_directory'); ?>/_img/icons/arrow-right-red.png" width="20" height="20"/ ></a>
                  </h2>
                  <img class="pdf" src="<?php bloginfo('template_directory'); ?>/_img/icons/pdf.png" width="32" height="32" />
                </div>
              </div>
              </tbody>
            </table>
          </div>
        <?php endif; ?>
      <?php endif;?>
    </article>
  </div>
<?php } ?>

<?php get_footer(); ?>

