<?php
/*
 *	Template Name: Chapter Child
 */
get_header(); ?>

<div class="row">
<?php
	if ( is_subpage() ) {
		$chapterslug = the_parent_slug();
		echo '<h1 class="head-alt"><a href="' . get_permalink(is_subpage()) . '"><i class="icon-arrow-left"></i>';
		if ( $chapterslug == 'queensland' ) {
			echo 'Queensland';
		} elseif ( $chapterslug == 'newcastle' ) {
			echo 'Newcastle';
		} elseif ( $chapterslug == 'sydney' ) {
			echo 'Sydney';
		} elseif ( $chapterslug == 'victoria' ) {
			echo 'Victoria';
		} elseif ( $chapterslug == 'tasmania' ) {
			echo 'Tasmania';
		} elseif ( $chapterslug == 'south-australia-and-nt' ) {
			echo 'South Australia &amp; NT';
		} elseif ( $chapterslug == 'western-australia' ) {
			echo 'Western Australia';
		}
		echo ' Chapter</a></h1>';
	}
?>

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<article class="main">
			<h1 class="page-title"><?php the_title(); ?></h1>
			<?php the_content(); ?>
		</article>
	<?php endwhile; ?>
	<?php get_sidebar('chapter'); ?>
</div>
<?php get_footer(); ?>