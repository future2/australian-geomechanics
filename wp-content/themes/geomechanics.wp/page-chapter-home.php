<?php
/*
 *	Template Name: Chapter Home
 */
get_header();
?>

<div class="row">
  <?php if ( have_posts() )
  while ( have_posts() ) : the_post(); ?>
  <h1 class="head-chapter"><?php the_title(); ?></h1>
  <section class="span9">

    <?php if($post->post_content != ''): ?>
      <div class="chapter-introduction">
        <?php the_content(); ?>
      </div>
    <?php endif; ?>
    <?php endwhile; wp_reset_query(); ?>

    <?php
    $chapterName = str_replace("-", " ", $post->post_name);
    if($chapterName == "South Australia") $chapterName = 'South Australia and NT';
    $chapterSlug = strtolower(str_replace(" ", "-", str_replace("&", "and", $chapterName)));
    $generalCatID = get_cat_ID('general');

    if(is_user_logged_in()){
      ?>
      <div class="box box-dash margin-bottom-45">
        <h1 style="margin-left:20px"><?php echo $chapterName ?> Chapter Updates</h1>
        <div class="box box-updates">
          <table style="width:100%">
            <?php
            $chapterNews = new WP_Query(
              array(
                'post_type'        => 'post',
                'posts_per_page'   => 6,
                'meta_key'         => 'meeting_start',
                'order'            => 'DESC',
                'orderby'          => 'meta_value',
                'category_name'    => $chapterSlug,
                'category__not_in' => array($generalCatID)
              )
            );

            if($chapterNews->have_posts()) {
              while($chapterNews->have_posts()) {
                $chapterNews->the_post();
                $post = get_post(get_the_ID());
                $tag = '';
                $postCats = get_the_category();
                $tagName = "error";
                if(has_category('meeting')){
                  $postDate = get_post_meta($post->ID, 'meeting_start', true);
                  $postDate = date("d/m/Y", strtotime($postDate));
                } else {
                  $postDate = new DateTime($post->post_date);
                  $postDate->format('d/m/Y');
                }
                foreach ($postCats as $cat){
                  switch($cat->slug){
                    case 'meeting':
                    case 'video':
                    case 'download':
                    case 'news':
                    case 'event':
                    case 'job':
                    $tagName = $cat->slug;
                    break;

                    case 'course':
                      $tagName = 'event';
                      break;
                  }
                }
                ?>
                <tr>
                  <td class='tagcell'>
                    <span class="tag tag-<?php echo $tagName ?>"><?php echo ucfirst($tagName) ?></span>
                  </td>
                  <td class='datecell'>
                    <?php
                    echo $postDate;
                    ?>
                  </td>
                  <td>
                    <a href='<?php echo get_permalink($post->ID); ?>'</a>
                    <?php
                    $titleLength = 65;
                    if (strlen($post->post_title) > $titleLength) {
                      echo substr(the_title($before = '', $after = '', FALSE), 0, $titleLength) . '...';
                    } else {
                      the_title();
                    }
                    ?>
                  </td>
                </tr>
              <?php }
            } else { ?>
              <tr>
                <td class='tagcell'>
                  <span class="tag tag-general">No Updates Found</span>
                </td>
              </tr>
            <?php }
            wp_reset_postdata(); ?>
          </table>
        </div>
        <div class="box-link-see-more">
          <div class="link-history">
            <?php
            $chapterId = get_cat_ID($chapterName);
            $chapterLink = get_category_link($chapterId);
            ?>
            <h3 class="h2"><a href="<?php echo $chapterLink ?>">READ MORE</a></h3>
          </div>
        </div>
      </div>
    <?php } else { ?>
      <div class="chapter-updates">
        <h2>Notes & Updates</h2>

        <?php
        $chapterNews = new WP_Query(
          array(
            'post_type'        => 'post',
            'posts_per_page'   => 6,
            'category_name'    => $chapterSlug,
            'category__in' => array($generalCatID),
            'meta_query' => array(
              'relation' => 'OR',
              array( //check to see if date has been filled out
                'key' => 'meeting_start',
                'compare' => 'EXISTS'
              ),
              array( //if no date has been added show these posts too
                'key' => 'meeting_start',
                'compare' => 'NOT EXISTS'
              )
            ),
            'order'            => 'DESC',
            'orderby'          => 'meta_value post_date'
          )
        );

        if($chapterNews->have_posts()) { ?>
          <ul class="highlight chapter-list-notes">
            <?php
            while ($chapterNews->have_posts()) {
              $chapterNews->the_post(); ?>
              <li><span><a href="<?php the_permalink(); ?>"><?php echo date('d/m/Y') . ' - ' . get_the_title(); ?></a></span></li>
            <?php	} ?>
          </ul>
        <?php }	?>

        <a href="<?php echo wp_login_url(get_permalink()); ?>" title="Login">Please login to see further chapter updates</a>
      </div>
    <?php } ?>

    <ul class="chapter-contents">
      <?php
      $page = $post->ID;
      // if user is logged in and is committee member
      // exclude resource sub pages: 9040,9049,9050,9051,9052,9053,
      // Exclude resource sub pages (LIVE): 9123, 9125, 9127, 9129, 9131, 9133, 9135

      if(is_user_logged_in() && get_the_author_meta('committee-member', $current_user->ID) == 'committee-member')
        wp_list_pages('child_of=' . $page . '&title_li=&depth=1');
      else
        wp_list_pages('child_of=' . $page . '&title_li=&depth=1&exclude=9040,9049,9050,9051,9052,9053,9054,9123,9125,9127,9129,9131,9133,9135');
      ?>
      <li class="span3 link-committee-alt">
        <a href="<?php echo home_url().'/committee-members/'.$post->post_name;?>">Committee</a>
      </li>
      <?php
      if(!is_user_logged_in() && get_field('public_resources_page')){ ?>
        <li class="span3 link-resources-alt">
          <a href="<?php echo get_field('public_resources_page'); ?>">Resources</a>
        </li>
      <?php } ?>
    </ul>
  </section>
	
  <?php get_sidebar('chapter'); ?>
</div>
<?php get_footer(); ?>