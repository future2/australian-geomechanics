<?php
/*
 *	Template Name: Chapter Meeting Index
 */
get_header(); ?>
<div class="row">
<?php
	if (is_subpage()) {
		$chapterslug = the_parent_slug();
		echo '<h1 class="head-alt"><a href="' . get_permalink(is_subpage()) . '"><i class="icon-arrow-left"></i>';
		if ($chapterslug == 'queensland') {
			echo 'Queensland';
		} elseif ($chapterslug == 'newcastle') {
			echo 'Newcastle';
		} elseif ($chapterslug == 'sydney') {
			echo 'Sydney';
		} elseif ($chapterslug == 'victoria') {
			echo 'Victoria';
		} elseif ($chapterslug == 'tasmania') {
			echo 'Tasmania';
		} elseif ($chapterslug == 'south-australia-and-nt') {
			echo 'South Australia &amp; NT';
		} elseif ($chapterslug == 'western-australia') {
			echo 'Western Australia';
		}
		echo ' Chapter</a></h1>';
	}
	?>
	<article class="span12">
		<h1 class="page-title">Meetings</h1>
		<dl class="meetings-index list-accordion">
			<?php

			global $post;
			$chapter = str_replace('-', ' ', the_parent_slug());
			$chapter_cat_ID = get_cat_ID($chapter);
			$args = array(
				'orderby' => 'name',
				'order' => 'DESC',
				'child_of' => $chapter_cat_ID
			);
			$categories = get_categories($args);

			foreach ($categories as $category) {
				$cat_year = substr($category->name, 0, 4);
				echo '<dt class="meeting-year-item"><a class="link-meeting-year" href="' . get_category_link($category->term_id) . '" title="' . sprintf(__("View all posts in %s"), $category->name) . '" ' . '><strong>' . $cat_year . '</strong><span>' . $category->count . ' Meetings</span></a></dt>
				<dd>
				<table class="meeting-year-list">
					<thead>
						<tr><th class="meeting-head-date">Date</th><th class="meeting-head-detail">Meeting Details</th></tr>
					</thead>
					<tbody>';

				$year_args = array(
					'cat' => $category->term_id,
					'showposts' => '-1',
					'meta_key' => 'meeting_start',
					'orderby' => 'meta_value',
					'order' => 'ASC'
				);

				query_posts($year_args);
				if (have_posts()) while (have_posts()) : the_post();
					date_default_timezone_set('Australia/Sydney');
					$rawdate = get_post_meta($post->ID, 'meeting_start', true);
					$y = substr($rawdate, 0, 4);
					$m = substr($rawdate, 4, 2);
					$d = substr($rawdate, 6, 2);
					$rawdate = $d . '-' . $m . '-' . $y;
					$eventDate = date("d M", strtotime($rawdate));
					$eventLink = get_permalink($post->ID);
					$eventTitle = get_the_title($post->ID);
					$eventSpeaker = get_post_meta($post->ID, "meeting_speaker", true);

					echo '<tr><td class="meeting-table-date"><a href="' . $evenLink . '">' . $eventDate . '</a></td><td class="meeting-table-detail"><a href="' . $eventLink . '"><span class="meeting-table-title">' . $eventTitle . '</span> <span class="meeting-table-speaker">' . $eventSpeaker . '</span></a></td></tr>';

				endwhile;

				echo '</tbody></table></dd>';
			}

			?>
		</dl>
	</article>
</div>
<?php get_footer(); ?>