<?php
/**
 * Created by PhpStorm.
 * User: ronaldchau
 * Date: 22/06/15
 * Time: 10:23 AM
 * Template Name: Committee Zone
 */
get_header();
?>

<div class="row">
    <h1 class="span12">Sydney Committee Home</h1>
    <section class="span9 main">
      <div class="box box-dash">
        <div class="box-promo box-content">
          <?php if (have_posts()) {
            while (have_posts()) {
              the_post();
              ?>
                <?php the_content(); ?>
            <?php } // end while loop
            } // end if
            wp_reset_query(); ?>
        </div>
      </div>
      <div class="row">
        <div class="span3 box box-icon">
          <img width=70 height=70 src="<?php bloginfo('template_directory');?>/_img/icons/ags-meetings-icon.svg">
          <h2 class="h2-black-alt">Meetings</h2>
        </div>
        <div class="span3 box box-icon">
          <img width=70 height=70 src="<?php bloginfo('template_directory');?>/_img/icons/ags-com-directory-icon.svg">
          <h2 class="h2-black-alt">Committee Directory</h2>
        </div>
        <div class="span3 box box-icon">
          <img width=70 height=70 src="<?php bloginfo('template_directory');?>/_img/icons/ags-resources-icon.svg">
          <h2 class="h2-black-alt">Resources</h2>
        </div>
      </div>
    </section>
    <section class="span3 sidebar">
      <div class="box box-next-meeting">
        <h2>NEXT MEETING</h2>
        <p>
          <a href="#">
            <span class="next-meeting-date">14 MAY 2015</span>
            <span class="next-meeting-title">54th Rankin Lecture (2014): Interactions in Offshore Foundation Design</span>
            <span class="next-meeting-speaker">Professor Guy Houlsby, University of Oxford</span>
          </a>
        </p>
      </div>
    </section>
</div>
<?php get_footer(); ?>