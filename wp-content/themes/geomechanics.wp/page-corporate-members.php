<?php
/*
	Template Name: Corporate Members
 */
get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<h1><?php the_title(); ?></h1>
<div class="row">
	<article id="post-<?php the_ID(); ?>" <?php post_class('introduction'); ?>>
		<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link">Pages:', 'after' => '</div>' ) ); ?>
	</article><!-- #post-## -->
	<?php endwhile; ?>
	<ul class="row corporate-members clearfix">
<?php // Select all posts in category 5 (corporate members) ** 10 in live **
	$member_query = new WP_Query('cat=10&showposts=-1&orderby=title&order=ASC');
	while ( $member_query->have_posts() ) : $member_query->the_post();
?>
		<li class="span4">
			<h3><?php the_title(); ?></h3>
			<?php the_content(); ?>
		</li>
<?php
	endwhile;
	wp_reset_postdata();
?>
	</ul>
</div>
<?php get_footer(); ?>