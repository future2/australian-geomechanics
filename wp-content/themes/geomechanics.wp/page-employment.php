<?php
/*
 *	Template Name: Employment
 */
get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<h1><?php the_title(); ?></h1>
<div class="row">
	<article id="post-<?php the_ID(); ?>" <?php post_class('span7'); ?>>
		<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link">Pages:', 'after' => '</div>' ) ); ?>
	</article><!-- #post-## -->
<?php
	endwhile;
	wp_reset_query();
	wp_reset_postdata();
?>
	<?php get_sidebar('employment'); ?>
</div>
<div class="row">
	<table class="span12 list-jobs">
		<thead>
			<tr>
				<td class="position"><strong>Position</strong></td>
				<td class="employer"><strong>Employer/Agent</strong></td>
				<td class="location"><strong>Location</strong></td>
				<td class="date"><strong>Date posted</strong></td>
			</tr>
		</thead>
		<tbody>
			<?php query_posts('cat=9&posts_per_page=-1&orderby=date&order=DSC'); ?>
			<?php while (have_posts()) : the_post(); ?>
			<tr>
				<td class="position"><a href="<?php echo get_post_meta($post->ID, 'job_file', true) ?>"><?php the_title(); ?></a></td>
				<td class="employer"><?php echo get_post_meta($post->ID, 'job_employer', true) ?></td>
				<td class="location"><?php echo get_post_meta($post->ID, 'job_location', true) ?></td>
				<td class="date"><?php echo get_post_meta($post->ID, 'job_date', true) ?></td>
			</tr>
			<?php endwhile; ?>
		</tbody>
	</table>

	<section class="span12 employment-sponsor">
		<h2>Sponsors:</h2>
		<ul>
			<li><a target="_blank" href="http://www.cardno.com.au/cu&amp;n/index.html"><img src="<?php echo get_bloginfo('template_directory'); ?>/_img/logoCardno.gif" width="166" height="30" alt="Cardno Ullman &amp; Nolan" /></a></li>
		</ul>
	</section>

</div>
<?php get_footer(); ?>