<?php
/*
  Template Name: Home Page
 */
get_header();
?>

<div class="row">

  <section class="top-banner span12">
    <?php if(get_field('banner_1_image_url')){ ?>
      <a href="<?php echo get_field('banner_1_link_url') ?>" target="_blank">
        <img class="size-full wp-image-10288 alignnone" src="<?php echo get_field('banner_1_image_url') ?>" alt="Custom Banner 1">
      </a>
    <?php } ?>
    <?php if(get_field('banner_2_image_url')){ ?>
      <a href="<?php echo get_field('banner_2_link_url') ?>" target="_blank">
        <img class="size-full wp-image-10294 alignnone" src="<?php echo get_field('banner_2_image_url') ?>" alt="Custom Banner 2">
      </a>
    <?php } ?>
  </section>

  <section class="main-content span9">

    <!-- Welcome message -->
    <?php if (have_posts()) while (have_posts()) : the_post(); ?>
      <div class="box-info margin-bottom-45">
        <h3 class="h2">
          <?php echo get_the_title() ?><?php if(is_user_logged_in()) echo ', '.ucfirst($current_user->get('display_name')).'!';?>
        </h3>
        <?php the_content(); ?>
      </div>
      <?php
    endwhile;
    wp_reset_query();
    wp_reset_postdata();
    ?>

    <!-- Chapter News -->
    <?php
    if(is_user_logged_in()) {
      $userChapter = $current_user->chapter;
      if($userChapter){
        $chapterName = $userChapter;
        if($userChapter == "South Australia") $chapterName = 'South Australia & NT';
        $chapterSlug = strtolower(str_replace(" ", "-", str_replace("&", "and", $chapterName)));
        $generalCatID = get_cat_ID('general');
        ?>
        <div class="box box-dash margin-bottom-45">
          <h1 style="margin-left:20px"><?php echo $chapterName ?> Chapter Updates</h1>
          <div class="box box-updates">
            <table style="width:100%">
              <?php
              $chapterNews = new WP_Query(
                array(
                  'post_type'        => 'post',
                  'posts_per_page'   => 6,
                  'category_name'    => $chapterSlug,
                  'category__not_in' => array($generalCatID),
                  'meta_query' => array(
                    'relation' => 'OR',
                    array( //check to see if date has been filled out
                      'key' => 'meeting_start',
                      'compare' => 'EXISTS'
                    ),
                    array( //if no date has been added show these posts too
                      'key' => 'meeting_start',
                      'compare' => 'NOT EXISTS'
                    )
                  ),
                  'order'            => 'DESC',
                  'orderby'          => 'meta_value post_date'
                )
              );

              if($chapterNews->have_posts()) {
                while($chapterNews->have_posts()) {
                  $chapterNews->the_post();
                  $post = get_post(get_the_ID());
                  $tag = '';
                  $postCats = get_the_category();
                  $tagName = "error";
                  if(has_category('meeting')){
                    $postDate = get_post_meta($post->ID, 'meeting_start', true);
                    $postDate = date("d/m/Y", strtotime($postDate));
                  } else {
                    //$postDate = new DateTime($post->post_date);
                    //$postDate->format('d/m/Y');
                    $postDate = date('d/m/Y', strtotime($post->post_date));
                  }
                  foreach ($postCats as $cat){
                    switch($cat->slug){
                      case 'meeting':
                      case 'video':
                      case 'download':
                      case 'news':
                      case 'event':
                      case 'job':
                        $tagName = $cat->slug;
                        break;

                      case 'course':
                        $tagName = 'event';
                        break;
                    }
                  }
                  ?>
                  <tr>
                    <td class='tagcell'>
                      <span class="tag tag-<?php echo $tagName ?>"><?php echo ucfirst($tagName) ?></span>
                    </td>
                    <td class='datecell'>
                      <?php
                      echo $postDate;
                      ?>
                    </td>
                    <td>
                      <a href='<?php echo get_permalink($post->ID); ?>'</a>
                      <?php
                      $titleLength = 65;
                      if (strlen($post->post_title) > $titleLength) {
                        echo substr(the_title($before = '', $after = '', FALSE), 0, $titleLength) . '...';
                      } else {
                        the_title();
                      }
                      ?>
                    </td>
                  </tr>
                <?php }
              } else { ?>
                <tr>
                  <td class='tagcell'>
                    <span class="tag tag-general">No Updates Found</span>
                  </td>
                </tr>
              <?php }
              wp_reset_postdata(); ?>
            </table>
          </div>
          <div class="box-link-see-more">
            <div class="link-history">
              <?php
              $chapterId = get_cat_ID($chapterSlug);
              $chapterLink = get_category_link($chapterId);
              ?>
              <h3 class="h2"><a href="<?php echo $chapterLink ?>">READ MORE</a></h3>
            </div>
          </div>
        </div>
      <?php } ?>
    <?php } ?>

    <!-- General News -->
    <?php
    $generalCatID = get_cat_ID('general');
    $publicNews = new WP_Query(
      array(
        'post_type'        => 'post',
        'posts_per_page'   => 6,
        'category__in' => array($generalCatID),
        'meta_query' => array(
          'relation' => 'OR',
          array( //check to see if date has been filled out
            'key' => 'meeting_start',
            'compare' => 'EXISTS'
          ),
          array( //if no date has been added show these posts too
            'key' => 'meeting_start',
            'compare' => 'NOT EXISTS'
          )
        ),
        'order'            => 'DESC',
        'orderby'          => 'meta_value post_date'
      )
    );

    if ($publicNews->have_posts()) { ?>
      <div class="box box-dash margin-bottom-45">
        <h1 style="margin-left:20px">General News</h1>
        <div class="box box-updates">
          <table>
            <?php while($publicNews->have_posts()) {
              $publicNews->the_post();
              $post = get_post(get_the_ID());
              ?>
              <tr>
                <td class='tagcell'>
                  <span class="tag tag-general">General</span>
                </td>
                <td class='datecell'>
                  <?php
                  $postDate = new DateTime($post->post_date);
                  echo $postDate->format('d/m/Y');
                  ?>
                </td>
                <td>
                  <a href='<?php echo get_permalink($post->ID); ?>'</a>
                  <?php
                  if (strlen($post->post_title) > 65) {
                    echo substr(the_title($before = '', $after = '', FALSE), 0, 65) . '...';
                  } else {
                    the_title();
                  }
                  ?>
                </td>
              </tr>
            <?php } ?>
          </table>
        </div>
        <div class="box-link-see-more">
          <div class="link-history">
            <?php
            $generalId = get_cat_ID('general');
            $generalLink = get_category_link($generalId);
            ?>
            <h3 class="h2"><a href="<?php echo $generalLink ?>">READ MORE</a></h3>
          </div>
        </div>
      </div>
    <?php }
    wp_reset_postdata(); ?>

  </section>

  <section class="sidebar span3">

    <!-- Twitter Feed -->
    <div class="margin-bottom-45">
      <?php get_twitter_timeline_widget(); ?>
    </div>

    <!-- About / Join Box -->
    <?php
    if (is_user_logged_in()) {
      // 4 is post-id for "About Us" page
      ?>
      <div class="box box-about margin-bottom-45">
        <p>About the Australian Geomechanics Society</p>
        <a href="<?php echo get_permalink(4) ?>" class="btn btn-right">Read more</a>
      </div>
    <?php } else { // 8360 is post-id for "become an ags member" page ?>
      <div class="box box-join margin-bottom-45">
        <p>Become a member of the Australian Geomechanics Society</p>
        <a href="<?php echo get_permalink(8360) ?>" class="btn btn-right">Join now</a>
      </div>
    <?php } ?>

    <!-- Looping Gallery -->
    <?php
    get_template_part('loop', 'gallery');
    ?>

  </section>

  <section class="full-content span12">

    <!-- Journals - Logged in only -->
    <?php
    if(is_user_logged_in()) { ?>
      <h1>Journal</h1>
      <?php
      global $post;
      $journal_id = get_option('fj_feature_journal');
      $post = get_post($journal_id);
      setup_postdata($post); ?>
      <?php if(isset($post)): ?>
        <div class="section-light journal-header">
          <?php
          $meta_values  = get_post_meta($journal_id, '', true);
          $tags = get_the_tags();
          ?>
          <div class="alignleft">
            <?php if(get_field('journal_cover_image')): ?>
              <img src="<?php echo get_field('journal_cover_image'); ?>" />
            <?php endif; ?>
          </div>
          <div class="alignleft content-excerpt">
            <h2><a href="<?php echo get_permalink($journal_id); ?>"><?php echo get_the_title(); ?></a></h2>
            <?php if(get_field('journal_release_date')) {
              echo '<p class="journal-date">';
              // in YYmmdd format
              $releaseDate = DateTime::createFromFormat('Ymd', get_field('journal_release_date'));
              echo $releaseDate->format('F Y');
              echo '</p>';
            } ?>
            <p><?php the_content(); ?></p>
          </div>
        </div>
        <?php $journalFile = get_field('journal_file', $journal_id);
        if($journalFile): ?>
          <div class="issues">
            <div class="accordion-header"><h2>Issue</h2></div>
            <table class="in-this-issue">
              <tbody>
              <div class="accordion-result l-padding-40">
                <div class="entry">
                  <h2><a href="<?php echo $journalFile ?>" title="<?php echo get_field('journal_title', $journal_id); ?>">
                      <?php echo get_field('journal_title', $journal_id); ?>
                      <img class="arrow" src="<?php bloginfo('template_directory'); ?>/_img/icons/arrow-right-red.png" width="20" height="20"/ ></a>
                  </h2>
                  <img class="pdf" src="<?php bloginfo('template_directory'); ?>/_img/icons/pdf.png" width="32" height="32" />
                </div>
              </div>
              </tbody>
            </table>
          </div>
        <?php endif; ?>
        <div class="download-box">
          <div class="left">
            <a href="/articles/archive">
              <img src="<?php bloginfo('template_directory'); ?>/_img/icons/history.png" width="32" height="32" />
            </a>
          </div>
          <div class="journal-link">
            <a href="<?php echo get_post_type_archive_link('journals'); ?>">Browse all journal issues </a>
          </div>
        </div>
      <?php endif;?>
    <?php }
    wp_reset_query(); ?>

  </section>

</div>

<?php get_footer(); ?>
