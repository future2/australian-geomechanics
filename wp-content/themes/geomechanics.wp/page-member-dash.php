<?php
/*
  Template Name: Member Dashboard
 */
get_header();
?>
<div class="row">
  <article class="home-dashboard type-page span12">
    <section class="span9 main">
      <h3 class="h2">WELCOME, JOHNATHON!</h3>
      <?php
      //query_posts('cat=112&showposts=5');
      if (have_posts()) {
        while (have_posts()) {
          the_post();
          ?>
          <div class="box-promo">
            <?php the_content(); ?>
            <br>
          </div>
        <?php
        } // end while loop
      } // end if
      wp_reset_query();
      ?>
    </section>
    <section class="span3 sidebar">
      <div class="box box-courses">
        <h2>Courses</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tellus quam, suscipit in efficitur a, aliquam eget ipsum.</p>
        <a href="#" class="btn">Find Out More</a>
      </div>
    </section>
  </article>
</div>
<div class="row">
  <article class="type-page span12">
    <h1>Member Seminars</h1>
    <div class="box box-dash">
      <div class="box-content">
        <table class="link-table">
        <tbody>
        <tr>
          <td class="tagcell"><span class="tag tag-seminar">Seminar</span></td>
          <td class="datecell">99/99/3000</td>
          <td><a href="#">Lorem Ipsum:</a></td>
        </tr>
        </tbody>
      </table>
      </div>
      <div class="box-link-see-more">
        <div class="link-history">
          <h3 class="h2"><a href="#">SEE MORE</a></h3>
        </div>
      </div>
    </div>
  </article>
</div>
  <div class="row">
    <article class="type-page span12">
      <h1>Member Resources</h1>
      <div class="box box-dash">
        <div class="box-content">
          <table class="link-table">
          <tbody>
          <tr>
            <td class="tagcell"><span class="tag tag-resource">Resource</span></td>
            <td class="datecell">99/99/3000</td>
            <td><a href="#">Lorem Ipsum:</a></td>
          </tr>
          <tr>
            <td class="tagcell"><span class="tag tag-resource">Resource</span></td>
            <td class="datecell">99/99/3000</td>
            <td><a href="#">Lorem Ipsum:</a></td>
          </tr>
          <tr>
            <td class="tagcell"><span class="tag tag-resource">Resource</span></td>
            <td class="datecell">99/99/3000</td>
            <td><a href="#">Lorem Ipsum:</a></td>
          </tr>
          </tbody>
        </table>
        </div>
        <div class="box-link-see-more">
          <div class="link-history">
            <h3 class="h2"><a href="#">SEE MORE</a></h3>
          </div>
        </div>
      </div>
    </article>
  </div>
<div class="row">
  <article class="type-page span12">
    <h1>Journal</h1>
    <div class="box box-dash">
      <div class="box-content">
        <h2 class="h2-black-alt h2">LOREM IPSUM DOLOR SIT AMET</h2>
        <em>November 2015</em>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ultricies dapibus sapien aliquet posuere. Aliquam id nisi at leo cursus elementum vel vitae sapien. Morbi varius orci id elit sodales dignissim. Nunc vitae metus eget massa hendrerit fringilla. Cras placerat sem vitae lorem mollis imperdiet sed ut orci.</p>
        <h3 class="h2">IN THIS ISSUE</h3>
        <table class="in-this-issue">
          <tbody>
            <tr>
              <td>Road Engineering Goes Crunk</td>
              <td>Offshore Drilling</td>
            </tr>
            <tr>
              <td>Lorem Ipsum</td>
              <td>Poppes Dolor Amir</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="box-link-see-more">
        <div class="link-pdf">
          <h3 class="h2"><a href="#">DOWNLOAD ISSUE</a></h3>
        </div>
      </div>
    </div>
    <div class="box-link-see-more">
      <div class="link-history">
        <h3 class="h2"><a href="#">BROWSE ALL JOURNAL ISSUES</a></h3>
      </div>
    </div>
  </article>
</div>
<?php get_footer(); ?>