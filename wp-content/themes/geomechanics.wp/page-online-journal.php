<?php
/*
 *	Template Name: Online Journal Index
 */
get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<h1><?php the_title(); ?></h1>
<div class="row">
	<div class="main span12">

	<?php if ( !is_user_logged_in() ) { ?>
		<h1><?php the_title(); ?></h1>
		<p><a href="<?php echo wp_login_url(home_url()); ?>">Login</a> to read and download papers from the Australian Geomechanics Journal.</p>
	<?php } ?>


	<?php if ( is_user_logged_in() ) { ?>
		<h1><?php the_title(); ?></h1>
		<?php the_content(); ?>
	<?php } ?>	

	</div>
</div>
<?php endwhile; ?>
<?php get_footer(); ?>