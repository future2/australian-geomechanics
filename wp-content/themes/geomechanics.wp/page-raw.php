<?php
/*
 *	Template Name: Raw Page
 */
get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<h1><?php the_title(); ?></h1>
<div class="row">
	<?php the_content(); ?>
</div>
<?php endwhile; ?>
<?php get_footer(); ?>