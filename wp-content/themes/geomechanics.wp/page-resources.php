<?php
/*
 * Template Name: Resource Browser
 */
?>
<?php
  get_header();

  // For some reason - $current_user in the header is not recognised here
  global $current_user;
  get_currentuserinfo();
  $chapterslug = $post->post_name;

  // Page Variables
  $chapterslug = the_parent_slug();

if(!is_user_logged_in() || (get_the_author_meta('committee-member', $current_user->ID) != 'committee-member' && get_the_author_meta('national-committee-member', $current_user->ID) != 'national-committee-member')) {
    echo '<div class="row page-header"><section class="span14">';
    echo '<h1>You must be a committee member to access committee resources</h1>';
    echo '<p>Not a member? <a href="'. get_permalink(6609) .'">Join Us</a></p>';
    echo '</section></div>';
  } else {
    session_start();
    $sort_by = 'sortby';
    // Set session to default value if not been set yet
    if ($_SESSION[$sort_by] == null) {
      $_SESSION[$sort_by] = 'modified';
    }

    ?>
    <div class="row page-header">
      <section class="span14">
        <div class="breadcrumbs">
          <a class="pointer" href="<?php echo get_permalink(is_subpage()); ?>">Chapter Page
            <img src="<?php bloginfo('template_directory'); ?>/_img/icons/arrow-left-red.png" title="back"
                 alt="back arrow" width="20" height="20"/>
          </a>
        </div>
        <?php
        //Change this to parent
        $chapterName = '';
        if ($chapterslug == 'queensland') {
          $chapterName = 'Queensland';
        } elseif ($chapterslug == 'newcastle') {
          $chapterName = 'Newcastle';
        } elseif ($chapterslug == 'sydney') {
          $chapterName = 'Sydney';
        } elseif ($chapterslug == 'victoria') {
          $chapterName = 'Victoria';
        } elseif ($chapterslug == 'tasmania') {
          $chapterName = 'Tasmania';
        } elseif ($chapterslug == 'south-australia-and-nt') {
          $chapterName = 'South Australia and NT';
        } elseif ($chapterslug == 'western-australia') {
          $chapterName = 'Western Australia';
        } elseif ($chapterslug == 'national-committee') {
          $chapterName = 'National';
        }
        ?>
        <h1>Resources for <?php echo $chapterName ?> Chapter</h1>
        <?php if (is_user_logged_in()) { ?>
          <div class="btn-add-resource pull-right"><a
              href="<?php echo get_permalink(get_page_by_path('add-resources')->ID); ?>">Add Resource</a></div>
        <?php } ?>
      </section>
    </div>
    <div class="row">
      <section class="span14">
        <?php while (have_posts()): the_post(); ?>
          <div class="section-light search-section resource-search">
            <?php the_content(); ?>
            <div class="search-left">
              <img src="<?php bloginfo('template_directory'); ?>/_img/icons/search.png" width="32" height="32"/>
            </div>
            <div class="search-right">
              <form method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>">
                <label for="s" class="assist">Search</label><input type="text" class="field" name="s" id="s"
                                                                   placeholder="Search by topic">
                <input type="hidden" name="post_type" value="resources"/>
                <input type="submit" class="submit" name="submit" id="searchsubmit" value="Search">
              </form>
            </div>
          </div>
        <?php endwhile;
        wp_reset_query(); ?>
      </section>
    </div>
  <div class="row page-header">
    <section class="span14">
      <h2>Browsing all resources</h2>

      <div class="resource-sort pull-right">
        <span>Sort By </span>
        <?php
        // Save post data to session
        $_SESSION[$sort_by] = $_POST['resource-sort-by']; ?>
        <form id="sort-resources" method="post" action="<?php get_permalink(); ?>">
          <select name="resource-sort-by" id="sort-resources-list">
            <option value="modified" <?php if ($_SESSION[$sort_by] == 'modified') echo 'selected' ?>>Date (last
              updated)
            </option>
            <option value="author" <?php if ($_SESSION[$sort_by] == 'author') echo 'selected' ?>>Author</option>
            <option value="title" <?php if ($_SESSION[$sort_by] == 'title') echo 'selected' ?>>Title</option>
            <option value="category" <?php if ($_SESSION[$sort_by] == 'category') echo 'selected' ?>>Category</option>
          </select>
        </form>
      </div>
    </section>
    <section class="span14">
    <?php
    $args = array(
      'post_type' => 'resources',
      'posts_per_page' => -1,
      'tax_query' => array(
        array(
          'taxonomy' => 'chapter',
          'field' => 'name',
          'terms' => $chapterName
        ),
      ),
      'post_status' => array(
        'publish',
        'any'
      ),
      'orderby' => $_SESSION[$sort_by],
      'order' => 'ASC'
    );
    query_posts($args);
    if (have_posts()) : while (have_posts()) : the_post();
      ?>
      <div class="resource-item section-light">
        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

        <div class="content-excerpt"><?php the_excerpt(); ?></div>
        <h2><a href="<?php the_permalink(); ?>">READ MORE<img class="arrow"
                                                              src="<?php bloginfo('template_directory'); ?>/_img/icons/arrow-right-red.png"
                                                              width="20" height="20">
          </a></h2>

        <div class="resource-item-properties">
          Author: <?php echo get_the_author_meta('first_name') . ' ' . get_the_author_meta('last_name'); ?><br>
          <?php $termList = wp_get_post_terms($post->ID, 'resource_type');
          $termList = mixed_to_name_string($termList);
          if($termList != '') {?>
          Category: <?php echo $termList; ?><br>
          <?php } ?>
          Last updated: <?php the_modified_date(); ?><br>
          <?php $postTags = get_the_tags();
          $postTags = mixed_to_name_string($postTags);
          if($postTags != '') { ?>
          Tagged: <?php echo $postTags; }?>
        </div>
        <?php if (get_field('resource_file_attachment')): ?>
          Attached Files:
          <div class="resource-file">
            <a href="<?php the_field('resource_file_attachment'); ?>"><img class="pdf"
                                                                           src="<?php bloginfo('template_directory'); ?>/_img/icons/pdf.png"
                                                                           width="50" height="50">
              <?php the_field('resource_file_name'); ?></a>
          </div>
        <?php endif; ?>
      </div>
    <?php endwhile; endif;
    wp_reset_query(); ?>
  </section>
  </div>
  <!-- pagination here -->
  <?php ags_numeric_posts_nav(); ?>
<?php } ?>
<?php get_footer(); ?>