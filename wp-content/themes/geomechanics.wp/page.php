<?php
/*
 * The template for displaying all pages.
 */
get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<h1><?php the_title(); ?></h1>
<div class="row">
	<article id="post-<?php the_ID(); ?>" <?php post_class('main'); ?>>
		<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link">Pages:', 'after' => '</div>' ) ); ?>
	</article><!-- #post-## -->
	<?php endwhile; ?>
	<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>