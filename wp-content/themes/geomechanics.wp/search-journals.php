<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
get_header(); ?>
<div class="breadcrumbs">
  <a href="<?php echo get_post_type_archive_link('journals'); ?>">Browse all journals
    <img src="<?php bloginfo('template_directory'); ?>/_img/icons/arrow-left-red.png" title="back" alt="back arrow" width="20" height="20" />
  </a>
</div>

<h1 class="page-title"><?php _e( 'Search results', 'twentyten' ); ?></h1>

<div class="section-light search-section">
  <div class="search-left">
    <img src="<?php bloginfo('template_directory'); ?>/_img/icons/search.png" width="32" height="32" />
  </div>
  <div class="search-right">
    <form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
      <label for="s" class="assist">Search</label>
      <input type="text" class="field" name="s" id="s" placeholder="Search by topic">
      <input type="hidden" name="post_type" value="journals" />
      <input type="submit" class="submit" name="submit" id="searchsubmit" value="Search">
    </form>
  </div>
</div>
		<div id="container">
			<div id="content" role="main">
        <?php

        $meta_values  = get_post_meta(get_the_ID(), '', true);

        // Note: Should be using get_posts() instead!!
        // create 2 queries and merge them
        // this will put results found with tags at the top and results
        // found in content and title under that
        $sq = get_search_query();
        $query1 = new WP_Query(array(
          'post_type' => 'journals',
          'tag_slug__in' => $sq,
          'orderby' => 'date',
          'order' => 'DESC',
          'posts_per_page' => -1,
        ));

        $query2 = new WP_Query(array(
          'post_type' => 'journals',
          's' => $sq,
          'orderby' => 'date',
          'order' => 'DESC',
          'posts_per_page' => -1,
        ));

        // Search in the attached pdf
        // Looks for pdf_title_i, where there are only 6 attachments
        $query3 = new WP_Query();

        // Check for number of documents in journals
        $number_of_journals = 15; // arbitrary number - Cannot yet get the maximum number of document for journals from meta data.

        // Search for all journals with document $sq
        for($i = 1; $i <= $number_of_journals; $i++) {
          $queryMeta = new WP_Query(array(
            'post_type' => 'journals',
            'meta_query' => array(
              'relation' => 'OR',
              array(
                'key' => 'pdf_title_'.$i,
                'value' => $sq,
                'compare' => 'LIKE'
              )
            ),
            'orderby' => 'date',
            'order' => 'DESC',
            'posts_per_page' => -1,
          ));

          //if($queryMeta->posts != null) {
          //  print "<pre>";
          //  print_r($queryMeta->posts);
          //  print "</pre>";
          //}

          // if found populate journal
          if($queryMeta->posts != null) {
            if ($query3->posts == null)
              $query3->posts = $queryMeta->posts;
            else
              $query3->posts = array_merge($query3->posts, $queryMeta->posts);

            $query3->post_count += $queryMeta->post_count;
          }
        }

        //create new empty query and populate it
        $wp_query = new WP_Query();
        $wp_query->posts = array_merge( $query1->posts, $query2->posts );

        // if papers in journal are found add to array
        //if( $query3->posts != null)
        //  $wp_query->posts = array_merge( $wp_query->posts, $query3->posts);

        //populate post_count count for the loop to work correctly
        $wp_query->post_count = $query1->post_count + $query2->post_count;
        /*if($query3->posts != null) {
          $wp_query->post_count += $query3->post_count;
        }*/?>

        <h1 class="page-title"><?php printf( __( 'Journal Search results for: %s', 'twentyten' ), '<span>' . $sq . '</span>' ); ?></h1>
        <?php if ( $wp_query->have_posts() ) : ?>
          <!-- the journal loop -->
          <?php while( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
            <div class="section-light search-result">
              <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <h2 class="entry-title">
                  <img class="pdf" src="<?php bloginfo('template_directory'); ?>/_img/icons/pdf.png" width="22" height="22" />
                  <a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark">
                    <?php the_title(); ?>
                  </a>
                </h2>
                <p>Found in journal:</p>
                <?php
                  $tags = get_the_tags();
                  if($tags) {
                    $tagTitle = false;
                   foreach($tags as $tag)
                   {
                    if($tag->slug == strtolower($sq)) { ?>
                      <h3>
                        <a href="<?php the_permalink(); ?>"><?php echo $tag->name; ?>
                          <img class="pdf" src="<?php bloginfo('template_directory'); ?>/_img/icons/arrow-right-red.png" width="20" height="20" />
                        </a>
                      </h3>
                      <?php
                      $tagTitle = true;
                      break;
                    }
                   }
                  }
                  if(!$tagTitle) { ?>
                  <h3>
                    <a href="<?php the_permalink(); ?>">
                      <?php the_title(); ?>
                      <img class="pdf" src="<?php bloginfo('template_directory'); ?>/_img/icons/arrow-right-red.png" width="20" height="20" />
                    </a>
                  </h3>
                <?php } ?>
                <p><?php echo get_the_date('F Y'); ?>&nbsp;</p>
              </div>
            </div>
          <?php endwhile; ?>
          <!-- end of the journal loop -->

          <!-- pagination here -->
          <?php ags_numeric_posts_nav(); ?>

          <?php wp_reset_postdata(); ?>
        <?php else : ?>
          <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
        <?php endif; ?>

        <h1 class="page-title"><?php printf( __( 'Document Search results for: %s', 'twentyten' ), '<span>' . $sq . '</span>' ); ?></h1>
        <?php if ( $query3->have_posts() ) : ?>
          <!-- the document loop -->
          <?php
          unset($previousTitles);
          $previousTitles = array();
          while( $query3->have_posts() ) : $query3->the_post();
            // Loop through all the documents in the journal
            $meta_values  = get_post_meta(get_the_ID(), '', true);
            for($i = 1; $i <= $number_of_journals; $i++) {
              if(isset($meta_values['attached_pdf_' . $i][0])) {
                $currentTitle = $meta_values['pdf_title_' . $i][0];
                if(!in_array($currentTitle, $previousTitles) && stripos($currentTitle, $sq) !== FALSE) {
                  $previousTitles[] = $currentTitle;
                  $docTitle = $currentTitle;
                  $docLink =  $meta_values['attached_pdf_' . $i][0];
                  break;
                }
              }
            } ?>
            <div class="section-light search-result">
              <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <h2 class="entry-title">
                  <img class="pdf" src="<?php bloginfo('template_directory'); ?>/_img/icons/pdf.png" width="22" height="22" />
                  <a href="<?php echo $docLink; ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark">
                    <?php echo $docTitle; ?>
                  </a>
                </h2>
                <p>Found in journal:</p>
                <?php
                $tags = get_the_tags();
                if($tags) {
                  $tagTitle = false;
                  foreach($tags as $tag)
                  {
                    if($tag->slug == strtolower($sq)) { ?>
                      <h3>
                        <a href="<?php the_permalink(); ?>"><?php echo $tag->name; ?>
                          <img class="pdf" src="<?php bloginfo('template_directory'); ?>/_img/icons/arrow-right-red.png" width="20" height="20" />
                        </a>
                      </h3>
                      <?php
                      $tagTitle = true;
                      break;
                    }
                  }
                }
                if(!$tagTitle) { ?>
                  <h3>
                    <a href="<?php the_permalink(); ?>">
                      <?php the_title(); ?>
                      <img class="pdf" src="<?php bloginfo('template_directory'); ?>/_img/icons/arrow-right-red.png" width="20" height="20" />
                    </a>
                  </h3>
                <?php } ?>
                <p><?php echo get_the_date('F Y'); ?>&nbsp;</p>
              </div>
            </div>
          <?php endwhile; ?>
          <!-- end of the document loop -->

          <!-- pagination here -->
          <?php ags_numeric_posts_nav(); ?>

          <?php wp_reset_postdata(); ?>
        <?php else : ?>
          <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
        <?php endif; ?>
        <p class="end-search-content">Not what you were looking for?</p>
        <div class="download-box">
          <div class="left">
            <a href="/articles/archive">
              <img src="<?php bloginfo('template_directory'); ?>/_img/icons/history.png" width="32" height="32" />
            </a>
          </div>

          <div class="journal-link">
            <a href="<?php echo get_post_type_archive_link('journals'); ?>">Browse all journal issues </a>
          </div>
        </div>
			</div><!-- #content -->
		</div><!-- #container -->

<?php get_footer(); ?>
