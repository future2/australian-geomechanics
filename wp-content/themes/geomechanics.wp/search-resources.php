<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
  get_header();
?>
  <div class="row page-header">
    <section class="span14">
      <div class="breadcrumbs">
        <a class="pointer" onClick="history.go(-1)">Chapter Page
          <img src="<?php bloginfo('template_directory'); ?>/_img/icons/arrow-left-red.png" title="back" alt="back arrow" width="20" height="20" />
        </a>
      </div>
      <h1 class="page-title">Resources for Sydney Chapter</h1>
      <button class="btn-add-resource pull-right">Add Resource</button>
    </section>
  </div>
  <div class="row">
    <section class="span14">
      <?php if(have_posts()): the_post(); ?>
        <div class="section-light search-section resource-search">
          <?php //the_content(); ?>
          <div class="search-left">
            <img src="<?php bloginfo('template_directory'); ?>/_img/icons/search.png" width="32" height="32" />
          </div>
          <div class="search-right">
            <form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
              <label for="s" class="assist">Search</label>
              <input type="text" class="field" name="s" id="s" placeholder="Search by topic">
              <input type="hidden" name="post_type" value="resources" />
              <input type="submit" class="submit" name="submit" id="searchsubmit" value="Search">
            </form>
          </div>
        </div>
      <?php endif; wp_reset_query(); ?>
    </section>
  </div>
  <?php
    // create 2 queries and merge them
    // this will put results found with tags at the top and results
    // found in content and title under that

    //TODO: Check query per chapter
    $sq = get_search_query();
    $query1 = new WP_Query(array(
      'post_type' => 'resources',
      'tag_slug__in' => $sq,
      'posts_per_page' => -1,
    ));
    $query2 = new WP_Query(array(
      'post_type' => 'resources',
      's' => $sq,
      'posts_per_page' => -1,
    ));
  ?>
  <div class="row page-header">
    <section class="span14">
      <h2><?php printf( __( 'Search results for: %s', 'twentyten' ), '<span>' . $sq . '</span>' ); ?></h2>
      <div class="resource-sort pull-right">
        <span>Sort By </span>
        <select>
          <option value="last_updated">Date (last updated)</option>
          <option value="author">Author</option>
          <option value="title">Title</option>
          <option value="category">Category</option>
        </select>
      </div>
    </section>
    <?php
    //create new empty query and populate it with the other two
    $wp_query = new WP_Query();
    $wp_query->posts = array_merge( $query1->posts, $query2->posts );

    //populate post_count count for the loop to work correctly
    $wp_query->post_count = $query1->post_count + $query2->post_count;
    //pr($wp_query);
    if ( $wp_query->have_posts() ) : ?>
      <?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
        <section class="span14">
          <div class="resource-search-item section-light search-result">
            <h3><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h3>
            <div>Found in Resources:</div>
            <h2><a href="<?php the_permalink(); ?>">TITLE IPSUM DOLOR SIT AMET<img class="arrow" src="<?php bloginfo('template_directory'); ?>/_img/icons/arrow-right-red.png" width="20" height="20">
              </a></h2>
            <div class="resource-search-properties">
              <p><?php echo get_the_date('F Y'); ?>&nbsp;</p>
              Tagged: <?php $postTags = get_the_tags();
              echo mixed_to_name_string($postTags);
              ?>
            </div>
          </div>
        </section>
      <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
    <?php else : ?>
      <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
    <?php endif; ?>
  </div>
<!--  PAGINATIONS-->
<!--  <div class="row page-footer">-->
<!--    <section class="span14">-->
<!--      <div class="prev-page">-->
<!--        <button class="btn-pagination">PREV</button>-->
<!--      </div>-->
<!--      <div class="pip-page">-->
<!--        <button class="btn-pagination">1</button>-->
<!--        <button class="btn-pagination">2</button>-->
<!--      </div>-->
<!--      <div class="next-page pull-right">-->
<!--        <button class="btn-pagination">NEXT</button>-->
<!--      </div>-->
<!--    </section>-->
<!--  </div>-->
  <?php ags_numeric_posts_nav(); ?>
  <p class="end-search-content">Not what you were looking for?</p>
  <div class="download-box">
    <div class="left">
      <a href="/articles/archive">
        <img src="<?php bloginfo('template_directory'); ?>/_img/icons/history.png" width="32" height="32" />
      </a>
    </div>

    <div class="journal-link">
      <a href="<?php echo get_post_type_archive_link('journals'); ?>">Browse all resources</a>
    </div>
  </div>
<?php get_footer(); ?>