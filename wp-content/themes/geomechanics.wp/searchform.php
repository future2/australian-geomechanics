<?php
/**
 * Search Form
 */
?>
<div class="container search-bar">
	<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<label for="s" class="assist">Search</label>
		<input type="text" class="field" name="s" id="s" placeholder="Search terms">
		<input type="submit" class="submit" name="submit" id="searchsubmit" value="Search">
    <input type="hidden" name="post_type" value="post" />
    <!--<input type="hidden" name="searchcat" value="<?php echo esc_url( home_url( '/' ) ); ?>" />
    <ul class="radio searchoptions">
			<li><label><input type="radio" name="post_type" value="post" checked> Search all of australiangeomechanics.org</label></li>
			<li><label><input type="radio" name="post_type" value="journals"> Search <cite>Australian Geomechanics</cite> journal</label></li>
		</ul>-->
	</form>
</div>