<?php
/*
 * The default sidebar
 */
?>

<section class="span3">
	<?php // Join button
		if ( !is_user_logged_in() ) {
	?>
	<div class="link link-member">
		<i class="sprite sprite-join"></i>
		<a href="http://www.engineersaustralia.org.au/technical-societies/join-technical-society">Become a member of the AGS</a>
	</div>
	<?php } ?>

	<div class="box box-next-meeting">
		<h2>Next Meeting</h2>
		<?php
		$year = date('Y');
		global $post;
		if (the_parent_slug() == 'chapters') { // If Chapter Home
			$chapter = get_query_var('pagename');
			if ( !$chapter && $id > 0 ) {
				$post = $wp_query->get_queried_object();
				$chapter = $post->post_name;
			}
		} else {
			$chapter = the_parent_slug();
		}
		$catSlug = $chapter;
		$catObj = get_category_by_slug($catSlug);
		$catId = $catObj->term_id;

		query_posts('cat=' . $catId . '&showposts=-1&meta_key=meeting_start&orderby=meta_value&order=ASC');
		if ( have_posts() ) while ( have_posts() ) : the_post();
			$today = date("Ymd");
			$postdate = get_post_meta($post->ID, 'meeting_start', true);
			$f = 0;
			if ( intval($today) <= intval($postdate) ) {
				$f += 1;
				$rawdate = get_post_meta($post->ID, 'meeting_start', true);
				$y = substr($rawdate,0,4);
				$m = substr($rawdate,4,2);
				$d = substr($rawdate,6,2);
				$rawdate = $d.'-'.$m.'-'.$y;
				$eventDay = substr(date("l", strtotime($rawdate)),0,3);
				$eventDate = date("d M Y", strtotime($rawdate));
				?>
				<p><a href="<?php the_permalink(); ?>"><span class="next-meeting-date"><?php echo $eventDate; ?></span> <span class="next-meeting-title"><?php the_title(); ?></span> <span class="next-meeting-speaker"><?php echo get_post_meta($post->ID, 'meeting_speaker', true); ?></span></a></p>
			<?php	}
			if ( $f > 0)
				break;

			endwhile;
			wp_reset_query();
			wp_reset_postdata();

			if ($f == 0)
				echo '<p>No upcoming meetings</p>';
		?>
	</div>

	<?php get_template_part( 'loop', 'gallery' ); ?>
</section>