<?php
/*
 * Courses sidebar
 */
?>

<section class="sidebar">
	<?php // Join button
		if ( !is_user_logged_in() ) {
	?>
	<div class="link link-member">
		<i class="sprite sprite-join"></i>
		<a href="http://www.engineersaustralia.org.au/technical-societies/join-technical-society">Become a member of the AGS</a>
	</div>
	<?php } ?>
</section>