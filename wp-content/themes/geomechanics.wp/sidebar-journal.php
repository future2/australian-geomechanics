<?php
/*
 * The default sidebar
 */
?>

<section class="sidebar">
	<?php // Join button
		if ( !is_user_logged_in() ) {
	?>
	<div class="link link-member">
		<a href="http://www.engineersaustralia.org.au/technical-societies/join-technical-society">Become a member of the AGS</a>
	</div>
	<div class="box box-journal">
		<h1>Online Journal</h1>
		<p><a href="<?php echo wp_login_url(home_url()); ?>">Login</a> to read and download papers from the Australian Geomechanics Journal.</p>
	</div>
	<?php } ?>

	<?php
		if ( is_user_logged_in() ) {
	?>
	<div class="box box-journal">
		<h1>Online Journal</h1>
		<p><a href="<?php echo get_permalink(3656); ?>" class="cta">Read or download papers</a></p>
	</div>
	<?php } ?>

</section>