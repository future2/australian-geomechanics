<?php
/*
 * The default sidebar
 */
?>

<section class="sidebar">
	<div class="link link-member">
		<a href="<?php echo get_permalink(6); ?>">Become a member of the AGS</a>
	</div>
	<div class="link link-download">
		<a href="<?php echo get_permalink(6); ?>">Download the AGS Constitution</a>
	</div>

	<?php get_template_part( 'loop', 'gallery' ); ?>
	<?php get_twitter_timeline_widget(); ?>
</section>