<?php
/*
 * Sidebar for resources.
 */
?>

<section class="sidebar">
	<?php // Join button
		if ( !is_user_logged_in() ) {
	?>
	<div class="link link-member">
		<i class="sprite sprite-join"></i>
		<a href="http://www.engineersaustralia.org.au/technical-societies/join-technical-society">Become a member of the AGS</a>
	</div>
	<?php } ?>
	<div class="link link-awards">
		<i class="sprite sprite-awards"></i>
		<a href="<?php echo get_category_link(12); ?>" title="Awards &amp; Prizes">Awards &amp; Prizes</a>
	</div>
	<div class="link link-download">
		<i class="sprite sprite-download"></i>
		<a href="<?php echo get_permalink(87); ?>" title="Downloads and papers">Downloads and papers</a>
	</div>
	<div class="link link-events">
		<i class="sprite sprite-events"></i>
		<a href="<?php echo get_category_link(5); ?>" title="Events">Events</a>
	</div>
	<div class="link link-links">
		<i class="sprite sprite-link"></i>
		<a href="<?php echo get_permalink(96); ?>" title="Related Links">Related Links</a>
	</div>
	<div class="link link-video">
		<i class="sprite sprite-video"></i>
		<a href="<?php echo get_permalink(90); ?>" title="Videos">Videos</a>
	</div>
	<div class="link link-training">
		<i class="sprite sprite-training"></i>
		<a href="<?php echo get_permalink(8498); // This is a temporary fix: echo get_category_link(6); ?>" title="Courses &amp; Training">Courses &amp; Training</a>
	</div>

	<?php get_template_part( 'loop', 'gallery' ); ?>
</section>