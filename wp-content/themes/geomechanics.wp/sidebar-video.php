<?php
/*
 * Sidebar for resources.
 */
?>

<section class="sidebar">
	<div class="button" id="btnAwards"> 
		<a href="<?php echo get_category_link(12); ?>" title="Awards &amp; Prizes">Awards &amp; Prizes</a>
	</div>
	<div class="button" id="btnDownloads"> 
		<a href="<?php echo get_permalink(87); ?>" title="Downloads and papers">Downloads and papers</a>
	</div>
	<div class="button" id="btnEvents">
		<a href="<?php echo get_category_link(5); ?>" title="Events">Events</a>
	</div>
	<div class="button" id="btnLinks">
		<a href="<?php echo get_permalink(92); ?>" title="Related Links">Related Links</a>
	</div>
	<div class="button" id="btnVideos">
		<a href="<?php echo get_permalink(90); ?>" title="Videos">Videos</a>
	</div>
	<div class="button" id="btnCourses">
		<a href="<?php echo get_category_link(8); ?>" title="Courses &amp; Training">Courses &amp; Training</a>
	</div>
	<div class="button" id="btnMemberInfo">
		<a href="http://www.engineersaustralia.org.au/technical-societies/join-technical-society">Become a member of the AGS</a>
	</div>
	
	<div class="note">
		<h3>A note on video</h3>
		<p>These videos stream in native HTML5 video formats and can be viewed with the browser on your computer or mobile device. If you experience difficulty loading the videos please ensure that you are using a modern web browser such as <a href="http://www.mozilla.com/">Firefox</a>, <a href="http://www.google.com/chrome/">Chrome</a> or <a href="http://www.apple.com/safari/download/">Safari</a> or have updated your <a href="http://www.adobe.com/products/flashplayer/">Adobe Flash Player</a> for Internet Explorer.</p>
	</div>

	<?php get_template_part( 'loop', 'gallery' ); ?>

</section>