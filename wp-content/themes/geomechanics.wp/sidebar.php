<?php
/*
 * The default sidebar
 */
?>

<section class="sidebar span3">
	<?php // Join button
		if ( !is_user_logged_in() ) {
	?>
	<div class="link link-member">
		<i class="sprite sprite-join"></i>
		<a href="http://www.engineersaustralia.org.au/technical-societies/join-technical-society">Become a member of the AGS</a>
	</div>
	<?php } ?>
	<?php // Custom buttons for Overview sidebar
		if ( is_page( 'overview' ) ) {
	?>
	<div class="link link-download">
		<i class="sprite sprite-download"></i>
		<a href="http://australiangeomechanics.org/wp-content/uploads/2010/11/constitution.pdf" target="_blank">Download the AGS Constitution</a>
	</div>
	<div class="link link-download">
		<i class="sprite sprite-download"></i>
		<a href="http://australiangeomechanics.org/wp-content/uploads/2015/08/20120802-ISSMGE-AUSTLSN-REGION.pptx" target="_blank">The Australiasian Region &amp; the International Society - Harry G. Poulos, Coffey Geotechnics</a>
	</div>
	<?php } ?>

	<?php // Custom buttons for Membership sidebar
		if ( is_page( 'membership' ) ) {
	?>
	<div class="link link-download">
		<i class="sprite sprite-download"></i>
		<a href="http://australiangeomechanics.org/wp-content/uploads/2010/11/constitution.pdf" target="_blank">Download the AGS Constitution</a>
	</div>
	<div class="link link-info">
		<i class="sprite sprite-info"></i>
		<a href="<?php echo get_permalink(8); ?>">Corporate Members</a>
	</div>
	<div class="link link-committee">
		<i class="sprite sprite-committee"></i>
		<a href="<?php echo get_permalink(13); ?>">The AGS Committee</a>
	</div>
	<?php } ?>

	<?php // Custom buttons for Membership sidebar
		if ( is_page( 'downloads' ) ) {
	?>
	<div class="link link-awards">
		<i class="sprite sprite-awards"></i>
		<a href="<?php echo get_category_link(12); ?>" title="Awards &amp; Prizes">Awards &amp; Prizes</a>
	</div>
	<div class="link link-events">
		<i class="sprite sprite-events"></i>
		<a href="<?php echo get_category_link(5); ?>" title="Events">Events</a>
	</div>
	<div class="link link-links">
		<i class="sprite sprite-links"></i>
		<a href="<?php echo get_permalink(96); ?>" title="Related Links">Related Links</a>
	</div>
	<div class="link link-video">
		<i class="sprite sprite-video"></i>
		<a href="<?php echo get_permalink(90); ?>" title="Videos">Videos</a>
	</div>
	<div class="link link-course">
		<i class="sprite sprite-training"></i>
		<a href="<?php echo home_url().'/course_post/'; // This is a temporary fix: echo get_category_link(6); ?>" title="Courses &amp; Training">Courses &amp; Training</a>
	</div>
	<?php } ?>

	<?php get_template_part( 'loop', 'gallery' ); ?>
	<?php get_twitter_timeline_widget(5); ?>
</section>