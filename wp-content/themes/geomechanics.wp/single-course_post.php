<?php
/**
 * Created by PhpStorm.
 * User: ronaldchau
 * Date: 22/06/15
 * Time: 2:18 PM
 */
get_header();
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

  <div class="row">
    <div class="main">
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="hgroup">
          <h1><?php the_title(); ?></h1>
        </div>
        <div class="entry-content">
          <?php the_content(); ?>
        </div>
      </article>
    </div>

    <section class="sidebar" style="margin-top:35px">

      <?php if(get_field('is_session_tba')){ ?>
        <div class="box box-meeting meeting-date">
          <h3 class="h2-black-alt">TBA</h3>
          <p><?php echo get_field('course_session'); ?></p>
        </div>
      <?php } ?>

      <?php if(have_rows('course_session') && count(get_field('course_session')) > 1){ ?>
        <section class="ac-container">
          <div class="box box-meeting">

            <?php
            $idx = 0;
            while(have_rows('course_session')){
              the_row();

              // Format and display time and date
              $timeFormat = 'g:ia';
              $dateFormat = 'D d M Y';

              // Use this to display date
              $dateDisplay = '';
              $timeDisplay = '';

              // Date
              $endDate =    DateTime::createFromFormat('dmY', get_sub_field('session_end_date'));
              $startDate =  DateTime::createFromFormat('dmY', get_sub_field('session_start_date'));

              // Time
              $startTime =  DateTime::createFromFormat($timeFormat, get_sub_field('session_start_time'));
              $endTime =    DateTime::createFromFormat($timeFormat, get_sub_field('session_end_time'));

              if($startDate != null)
                $dateDisplay .= $startDate->format($dateFormat);
              if($endDate != null)
                $dateDisplay .= ' - '.$endDate->format($dateFormat);

              if($startTime != null)
                $timeDisplay .= $startTime->format($timeFormat);
              if($endTime != null)
                $timeDisplay .= ' - '.$endTime->format($timeFormat);

              $location = get_sub_field('session_venue_name');
              $address = get_sub_field('session_venue_address');

              $registrationForm = get_sub_field('session_registration_form');

              ?>

              <div>
                <input id="ac-<?php echo $idx ?>" name="session_times" type="radio" <?php echo (!$idx ? 'checked=""' : 'no') ?>>
                <label for="ac-<?php echo $idx ?>"><?php echo $location; ?></label>
                <article>

                  <div class="meeting-date">
                    <span>
                      <?php echo $dateDisplay; ?>
                    </span>
                  </div>

                  <div class="meeting-time">
                    <span>
                      <?php echo $timeDisplay; ?>
                    </span>
                  </div>

                  <div class="meeting-location">
                    <span>
                      <?php echo $location; ?>
                      <br />
                      <?php echo $address; ?>
                    </span>
                  </div>

                  <?php if($registrationForm){ ?>
                    <div class="meeting-flyer">
                      <span>
                        <a href="<?php echo $registrationForm ?>" download>Registration Form</a>
                      </span>
                    </div>
                  <?php } ?>

                </article>
              </div>
              <?php
              $idx++;
            } ?>
          </div>
        </section>
      <?php } else if(have_rows('course_session')){ ?>
        <div class="box box-meeting">
          <?php while(have_rows('course_session')){
            the_row();

            // Format and display time and date
            $timeFormat = 'g:ia';
            $dateFormat = 'D d M Y';

            // Use this to display date
            $dateDisplay = '';
            $timeDisplay = '';

            // Date
            $endDate =    DateTime::createFromFormat('dmY', get_sub_field('session_end_date'));
            $startDate =  DateTime::createFromFormat('dmY', get_sub_field('session_start_date'));

            // Time
            $startTime =  DateTime::createFromFormat($timeFormat, get_sub_field('session_start_time'));
            $endTime =    DateTime::createFromFormat($timeFormat, get_sub_field('session_end_time'));

            if($startDate != null)
              $dateDisplay .= $startDate->format($dateFormat);
            if($endDate != null)
              $dateDisplay .= ' - '.$endDate->format($dateFormat);

            if($startTime != null)
              $timeDisplay .= $startTime->format($timeFormat);
            if($endTime != null)
              $timeDisplay .= ' - '.$endTime->format($timeFormat);

            $location = get_sub_field('session_venue_name');
            $address = get_sub_field('session_venue_address');

            $registrationForm = get_sub_field('session_registration_form');

            ?>

            <div class="meeting-date">
              <span>
                <?php echo $dateDisplay; ?>
              </span>
            </div>

            <div class="meeting-time">
              <span>
                <?php echo $timeDisplay; ?>
              </span>
            </div>

            <div class="meeting-location">
              <span>
                <?php echo $location; ?>
                <br />
                <?php echo $address; ?>
              </span>
            </div>

            <?php if($registrationForm){ ?>
              <div class="meeting-flyer">
              <span>
                <a href="<?php echo $registrationForm ?>" download>Registration Form</a>
              </span>
              </div>
            <?php } ?>

          <?php } ?>
        </div>
      <?php } ?>

      <?php if(have_rows('information_boxes')){ ?>
        <?php while(have_rows('information_boxes')){
          the_row();

          $title    = get_sub_field('title');
          $text     = get_sub_field('text');
          $fileUrl  = get_sub_field('file');
          $fileTxt  = get_sub_field('file_text');
          $imageUrl = get_sub_field('image');
          $email    = get_sub_field('email');
          $icon     = get_sub_field('icon');

          ?>
          <div class="box box-meeting meeting-<?php echo $icon ?>">

            <?php if($title){ ?>
              <span><?php echo $title ?></span>
            <?php } ?>

            <?php if($text){ ?>
              <p><?php echo $text ?></p>
            <?php } ?>

            <?php if($fileUrl){ ?>
              <span><a href="<?php echo $fileUrl ?>" download><?php echo $fileTxt ?></a></span>
            <?php } ?>

            <?php if($imageUrl){ ?>
              <img src="<?php echo $imageUrl ?>" width="100%" />
            <?php } ?>

            <?php if($email){ ?>
              <span><a href="mailto:<?php echo $email ?>"><?php echo $email ?></a></span>
            <?php } ?>

          </div>
        <?php } ?>
      <?php } ?>

      <?php if(have_rows('course_enquires')){ ?>
        <div class="box box-meeting meeting-flyer" style="word-wrap: break-word;">
          <span><?php echo get_field('enquiries_title') ?></span>
          <?php while(have_rows('course_enquires')){
            the_row(); ?>
            <?php if(get_sub_field('contact_name')){ ?>
              <div><?php the_sub_field('contact_name'); ?></div>
            <?php } ?>
            <?php if(get_sub_field('contact_phone')){ ?>
              <div><strong>Phone: </strong><?php the_sub_field('contact_phone'); ?></div>
            <?php } ?>
            <?php if(get_sub_field('contact_email')){ ?>
              <div><strong>Email: </strong><a href="mail-to:<?php the_sub_field('contact_email'); ?>"><?php the_sub_field('contact_email'); ?></a></div>
        <?php } ?>
          <?php } ?>
        </div>
      <?php } ?>

    </section>
  </div>

<?php endwhile; ?>
<?php get_footer(); ?>