<?php
/**
 * The Template for displaying all single journals (plugin).
 */

get_header();
?>


<article class="type-page span12">
  <div class="breadcrumbs">
    <a class="pointer" onClick="history.go(-1)">Back to search results
      <img src="<?php bloginfo('template_directory'); ?>/_img/icons/arrow-left-red.png" title="back" alt="back arrow" width="20" height="20" />
    </a>
  </div>

  <?php
    $slug = get_query_var('journals');
    if($slug != '') {
      $wp_query = new WP_Query(array(
        'name' => $slug,
        'post_type' => 'journals',
        'post_status' => 'publish',
        'numberposts' => 1,
      ));
    }
  ?>

  <?php while ($wp_query->have_posts()) : $wp_query->the_post();?>
  <?php if(is_user_logged_in()) { ?>
  <h1><?php the_title(); ?></h1>

  <div class="section-light">
      <?php
        $meta_values  = get_post_meta(get_the_ID(), '', true);
        $tags = get_the_tags();
      ?>
      <h2><?php the_title(); ?></h2>
      <p class="journal-date"><?php echo get_the_date('F Y'); ?></p>
      <p><?php the_content(); ?></p>
      <?php if($tags): ?>
        <h3>Tags</h3>
        <?php foreach($tags as $tag) { ?>
          <span class="journal-tag"><?php echo $tag->name; ?></span>
        <?php } ?>
      <?php endif; ?>
  </div>
  <div class="issues">
    <div class="accordion-header">
      <h2>In this issue</h2>
    </div>
      <?php
      $x = 0;
      $i = 0;
      $number_of_journals = 0;
      while($x == 0) {
        $i++;
        if (array_key_exists('attached_pdf_' . $i, $meta_values)) {
          $number_of_journals++;
        } else {
          $x = 1;
        }
      }

      for($i = 1; $i <= $number_of_journals; $i++) { ?>
      <?php if(isset($meta_values['attached_pdf_' . $i][0])) { ?>
       <div class="accordion-result">
          <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="month">
              <?php echo $i ?>
            </div>
            <div class="entry">
              <h2>
                <a href="<?php echo $meta_values['attached_pdf_' . $i][0]; ?>" title="<?php echo $meta_values['pdf_title_' . $i][0]; ?>">
                  <?php echo $meta_values['pdf_title_' . $i][0]; ?>
                  <img class="arrow" src="<?php bloginfo('template_directory'); ?>/_img/icons/arrow-right-red.png" width="20" height="20" />
                  <img class="pdf" src="<?php bloginfo('template_directory'); ?>/_img/icons/pdf.png" width="32" height="32" />
                </a>
              </h2>
            </div>
          </div>
        </div>
      <?php } ?>
    <?php } ?>
  </div>
  <div class="download-box">
    <div class="left">
      <a href="/articles/archive">
        <img src="<?php bloginfo('template_directory'); ?>/_img/icons/history.png" width="32" height="32" />
      </a>
    </div>
    <div class="journal-link">
      <a href="<?php echo get_post_type_archive_link('journals'); ?>">Browse all journal issues</a>
    </div>
  </div>
  <?php } else {
      // you should never be able to get to here as the plugin denies
      // access unless logged in, however, this is here as a fallback for security.
      ?>
    <h2>Access denied</h2>
    <p>Please <a href="<?php echo wp_login_url(get_permalink(get_the_ID())); ?>">login</a> to view this journal.</p>
  <?php } ?>
  <?php endwhile;?>
</article>
<?php get_footer(); ?>
