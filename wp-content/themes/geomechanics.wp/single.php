<?php
/**
 * The Template for displaying all single posts.
 */

	get_header();
	// Custom Post Types
	//===============================================
	if(is_singular('committee-members')) {
		get_template_part('loop', 'committee-members');
	} else if(is_singular('resources')) {
		get_template_part('loop', 'resources');
	} else if(get_post_type() == 'course_post') {
		get_template_part('loop', 'course-post');
	}
	//===============================================
	else if ( in_category('meeting') ) {
		get_template_part( 'loop', 'meeting' );
	} elseif ( in_category('journal') ) {
		get_template_part( 'loop', 'journal' );
	} elseif ( in_category('video') ) {
		get_template_part( 'loop', 'video' );
	} elseif ( in_category('paper') ) {
		get_template_part( 'loop', 'paper' );
	} elseif ( in_category('courses') ) {
		get_template_part('loop', 'courses');
	} else {
		get_template_part( 'loop', 'general' );
	}

	get_footer();
